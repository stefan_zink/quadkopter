/*
 * ring_puffer.h
 *
 *  Created on: May 21, 2014
 *      Author: I8L-PC05-G01
 */

#ifndef RING_PUFFER_H_
#define RING_PUFFER_H_
#include "usart.h"
#include "structs.h"

#endif /* RING_PUFFER_H_ */

extern ringpuffer ringpuffer_A;
void insert_in_rbuffer(char c,ringpuffer* r);
char read_from_rbuffer(ringpuffer* r);
char read_from_buffer();
void Rpuffer_toDISP_raw(ringpuffer* A);
void clearRpuffer(ringpuffer* r);
int isAvailable(ringpuffer* r);
/**
 * USART INTTERUPT das des zeug in den puffer schreibt
 */
__attribute__((__interrupt__))
static void usart_int_handler(void)
{
 int c;	// Empfangenes Datum
 usart_read_char((&AVR32_USART0), &c);
 insert_in_rbuffer(c,&ringpuffer_A);
}
