/*
 * basics.c
 *
 *  Created on: 07.06.2014
 *      Author: Norbert
 */

void double_to_unsigchar(double d,unsigned char *c){
        if(d<0)c=0;
        else if(d>255)*c=255;
        else *c=(char)d;
}


