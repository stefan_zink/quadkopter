/*
 * basics.h
 *
 *  Created on: 12.04.2011
 *      Author: gageik
 */

#ifndef BASICS_H_
#define BASICS_H_

#define DEBUG_BUFFER_SIZE		200
#define RECEIVE_DATA_LENGTH		6
#define CPU_SPEED				12000000		// This needs to be the oscillator fequency

#define DEBUG_MSG_IMU_RAW
#define DEBUG_MSG_TWI

// Sensor Rohdaten
typedef struct {
	short aX;
	short aY;
	short aZ;
		//conditiones
	double aXcond;double aYcond;double aZcond;//Koditioniert
	//double aXKcond;double aYKcond;double aZKcond;//Kalibrierte Werte

	short gX;
	short gY;
	short gZ;
	double gXcond;double gYcond;double gZcond;
	//double gXKcond;double gYKcond;double gZKcond;
	//double sensor_2;
	// Das ist nur ein Beispiel: Bennent Sie besser!//vielleciht machen wir das auch0.o//haben wir sogar
} sensorDaten_raw;

void dip_write_string(char* string, int zeile);
void USART_schreiben(char* string);
void double_to_unsigchar(double d,unsigned char* c);
#endif /* BASICS_H_ */
