/*
 * BlCtrl.h
 *
 *  Created on: 09.12.2011
 *      Author: gageik
 */

#ifndef BLCTRL_H_
#define BLCTRL_H_

#define BLCTRL_ENGINE1_TWI_ADDRESS		0x29	// Engine TWI address
#define BLCTRL_ENGINE2_TWI_ADDRESS		0x2A
#define BLCTRL_ENGINE3_TWI_ADDRESS		0x2B
#define BLCTRL_ENGINE4_TWI_ADDRESS		0x2C

//#define BLCTRL_2

void set_engine(unsigned char Stellwert1, unsigned char Stellwert2, unsigned char Stellwert3, unsigned char Stellwert4);
void blctrl_init(void);

#endif /* BLCTRL_H_ */
