/*
 * route_calc.c
 *
 *  Created on: Jun 16, 2014
 *      Author: Norb
 */
#include "structs.h"
#include "route_calc.h"
#include <stdio.h>
#include "math.h"
#include "basics.h"
#ifndef ROUTE_CALC_C_
#define ROUTE_CALC_C_

//lel , doch keine liste^^

#define max_speed 4.0f
#define acc_time 2.0f
#define acc_max 1.2f
#endif /* ROUTE_CALC_C_ */

void PrintRout(rout* r,int p){
	char puff[120];
	sprintf(puff,"\n%d----ROUTING-----\n",p);
	USART_schreiben(puff);
	sprintf(puff,"A: %2lf %2lf B: %2lf %2lf\n",r->A.x,r->A.y,r->B.y,r->B.y);
	USART_schreiben(puff);
	sprintf(puff,"Dist: %lf\n",r->dist);//distance to fly
	USART_schreiben(puff);
	sprintf(puff,"SIF: %lf\n",r->SIF);//speed in flight
	USART_schreiben(puff);
	sprintf(puff,"SCF;%d STFF:%d STBF:%d STS:%d\n",r->sample_current_flight,r->sample_till_flightFloat,r->sample_till_breakFlight,r->sample_till_stop);
	USART_schreiben(puff);


}


double acc_to_way(double acc,double t);
/**
 * Da null basiert entspricht die schreib position -1 den zu benotigenden routen
 * 0-> 0-1 err
 * 1-> 1-1 err
 * 2-> 2-1 1 route
 * 3-> 3-1 2 route
 * ..
 * 5-> 5-1 4 routen
 */

unsigned char w_knot;
unsigned char r_knot;
waypoint waypoints[max_knots];
rout routs[max_knots-1];

void addKnot(int x,int y){
	waypoints[w_knot].x=x;
	waypoints[w_knot].y=y;
	w_knot++;
}
double deg_to_rad(double degrees){
	double erg=degrees * M_PI / 180.0;
	return erg;
}
double reg_to_deg(double radians){
	double erg=radians * 180.0 / M_PI;
	return erg;
}
void CalCulateSuperPostion(rout* r,regelungspara* para){

	double angle_err=r->angle-para->yaw_current;
	////printf("winkelfehler:%lf\n",angle_err);
	angle_err=deg_to_rad(angle_err);
	////printf("radiantenfehler:%lf\n",angle_err);
	double V[2]={0,1};//einheitsvektor der geschwindigkeit
	double Vr[2]={0,0};
	double M[2][2]={{ cos(angle_err) , -sin(angle_err)	},{ sin(angle_err) , cos(angle_err)     }};
	//print_matrix(M);
	Vr[0] = (M[0][0]*V[0])+(M[0][1]*V[1]);
    Vr[1] = (M[1][0]*V[0])+(M[1][1]*V[1]);
	para->nick_aim=Vr[0] * acc_angle_max;
	para->roll_aim=Vr[1] * acc_angle_max;

}

//distanz berechnen
double CalcDistance(waypoint A,waypoint B){
	double a=A.x-B.x;
	double b=A.y-B.y;
	double c= (a * a) + (b * b);
	c=sqrt(c);
	return c;
}


double oneDimRout(double a,double b){
	waypoint T1={0,a};
	waypoint T2={0,b};
	double dist=CalcDistance(T1,T2);
	return dist;
}

//maximalbeschleunigung berechnen
double calc_max_acc_t(double a,double b){
	double c=oneDimRout(a,b);
	c/=2;//strecken abschnitte die zur verfuegung stehe

	if(	c >= max_speed/acc_max ){//kann mit standard faktor beshcleunigen
		//printf(":Stat");
		c=max_speed;
		c/=acc_max;

	}
	else{
		c=(max_speed*0.25);//anfliegen der geschwindigkeit , mal n bissl runterskalieren, is net optimal , is nur mal geraten, aber yäi, is sogar ein m/s, aber wieso ? k.a. xD
		c/=acc_max;
	}
return c;
}
//winkel zwischen zwei vektoren
double VektorAngle(double A[2],double B[2]){
		double angle_toTurn=    (A[0]*B[0])       + (A[1] * B[1]) ;
		angle_toTurn/= (        sqrt(   ((A[0]*A[0])+(A[1]*A[1]))   )  *  sqrt(     ((B[0]*B[0])+(B[1]*B[1]))   )       );
        angle_toTurn=acos(angle_toTurn);
        angle_toTurn= 180 / M_PI * angle_toTurn;
        return angle_toTurn;
}

//winkelberechnung um welchen winkel man
double calculateTurnAngle(waypoint A,waypoint B){
	double VektorA[2];
	double VektorL[2];
	VektorA[0]=B.x-A.x;
	VektorA[1]=B.y-A.y;
	VektorL[0]=1;//+(A.x*A.x);
	VektorL[1]=0;//sigh ja es kann zu nen null vektor kommen -.-
	double angle_toTurn= VektorAngle(VektorA,VektorL);
	return angle_toTurn;//wegen einheitsvektor
}

double acc_to_way(double acc,double t){
	double s=0.5 *  acc;
	s*=t*t;
	//s=fabs(s);
	return s;
}

//berechnet alle routen
void ParseRouts(int sampletime){
int i=0;
for(i=0;i<w_knot-1;i++){
		////printf("kk:%lf,%lf;%lf,%lf\n",waypoints[i].x,waypoints[i].y,waypoints[i+1].x,waypoints[i+1].y);
double angle=calculateTurnAngle(waypoints[i], waypoints[i+1]);
double dist=CalcDistance(waypoints[i],waypoints[i+1]);
	double VektorA[2];
        VektorA[0]=waypoints[i+1].x-waypoints[i].x;
        VektorA[1]=waypoints[i+1].y-waypoints[i].y;
        double acc_t=calc_max_acc_t(VektorA[0],VektorA[1]);
        double acc_way=acc_to_way(acc_max,acc_t);
        double way_stillTodo=dist-(acc_way*2);//strecke minus beschleunigungs und bremsweg
        double break_in_m=way_stillTodo;//meter nach denen gebremstwird
        //if(break_in_m<0)printf("lelfgtfg");
        double speedInFlight=acc_t*acc_max;
        double breaking_start_time=acc_t+ (break_in_m / speedInFlight);
        double stop=acc_t  + breaking_start_time;
        //char debug_CalCulations[200];
        //sprintf(debug_CalCulations,"ROUT::%d\n::acc_t:%.2lf\n::t_stf:%.2lf\n::t_ss:%.2lf\n",i,acc_t,breaking_start_time,stop);
        //USART_schreiben(debug_CalCulations);
        routs[i].angle=angle;
        routs[i].A=waypoints[i];
        routs[i].B=waypoints[i+1];
        routs[i].SIF=speedInFlight;
        routs[i].dist=dist;
        routs[i].sample_current_flight=0;
        routs[i].sample_till_flightFloat=((acc_t*1000)/sampletime);
        routs[i].sample_till_breakFlight=((breaking_start_time*1000)/sampletime);
        routs[i].sample_till_stop=((stop*1000)/sampletime);
        //sprintf(debug_CalCulations,"SS:ROUT::%d\n::acc_t:%d\n::t_stf:%d\n::t_ss:%d\n",i,routs[i].sample_till_flightFloat,routs[i].sample_till_breakFlight,routs[i].sample_till_stop);
        //USART_schreiben(debug_CalCulations);
        r_knot++;
}
}





