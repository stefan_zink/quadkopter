/*
 * coreconfig.c
 *
 *  Created on: 04.06.2014
 *      Author: Norbert
 *      Beinhaltet Grundeinstellungen die spaeter mal ausgewaelt werden koennen
 */
#include "coreconfig.h"
#include "structs.h"
#include "AVR_Framework.h"
#include "basics.h"
#include "stdio.h"
#include "tc_ng.h"
#define configs_num 7
#define default_one 6
extern unsigned char PB[3];
extern unsigned char PB_b[3];
base_config configs[configs_num];
extern unsigned int tc_ticks;
void ResetPBs();
void Config_via_UART(base_config* c);
void Config_via_CODE(base_config* c);
void Config_Assitent(base_config* c);
void assign_configs();
int select_config(int i,base_config* b);
char dip_mesg[30];
extern ringpuffer ringpuffer_A;
/**
 * Weist die Konfigurationen den slots zu
 */
void assign_configs(){
	int i=0;
	sprintf(configs[i].configname,"Quadro%d",i);
		configs[i].Pa=2.68;configs[i].Da=0.413;configs[i].Ia=0.00;
		configs[i].Py=1.0;configs[i].Dy=0.00;configs[i].Iy=0.00;
		configs[i].P_acc=3.0;configs[i].P_gyro=1.0;configs[i].M_acc=7000000.0;configs[i].M_gyro=0.001;
		configs[i].base_speed=90;
		configs[i].sample_time=15;
		configs[i].fpt_limit=7;
		configs[i].kalibrierungsanzahl=450;
	i++;//Aktuelle Configuration ..
	sprintf(configs[i].configname,"Quadro%d",i);
		configs[i].Pa=3.00;configs[i].Da=0.55;configs[i].Ia=0.2;
		configs[i].Py=1.0;configs[i].Dy=0.09;configs[i].Iy=0.0;
		configs[i].P_acc=3.0;configs[i].P_gyro=1.0;configs[i].M_acc=7000000.0;configs[i].M_gyro=0.001;
		configs[i].base_speed=60;
		configs[i].sample_time=15;
		configs[i].fpt_limit=7;
		configs[i].kalibrierungsanzahl=450;
	i++;//TODO Werte einstellen
	sprintf(configs[i].configname,"Abgabe 1");
		configs[i].Pa=3.50;configs[i].Da=0.5;configs[i].Ia=0.2;
		configs[i].Py=1.00;configs[i].Dy=0.09;configs[i].Iy=0.0;
		configs[i].P_acc=3.0;configs[i].P_gyro=1.0;configs[i].M_acc=10000000.0;configs[i].M_gyro=0.001;
		configs[i].base_speed=60;
		configs[i].sample_time=15;
		configs[i].fpt_limit=7;
		configs[i].kalibrierungsanzahl=250;
	i++;//TODO Werte einstellen
		sprintf(configs[i].configname,"Abgabe 2");
			configs[i].Pa=2.2;configs[i].Da=0.7;configs[i].Ia=0.4;
			configs[i].Py=1.00;configs[i].Dy=0.2;configs[i].Iy=0.0;
			configs[i].P_acc=3.0;configs[i].P_gyro=1.0;configs[i].M_acc=100000.0;configs[i].M_gyro=0.001;
			configs[i].base_speed=80;
			configs[i].sample_time=15;
			configs[i].fpt_limit=7;
			configs[i].kalibrierungsanzahl=250;
	i++;//TODO Werte einstellen //das 4.
		sprintf(configs[i].configname,"Attitude ang");
			configs[i].Pa=2.0;configs[i].Da=0.5;configs[i].Ia=0.4;
			configs[i].Py=1.00;configs[i].Dy=0.2;configs[i].Iy=0.0;
			configs[i].P_acc=3.0;configs[i].P_gyro=1.0;configs[i].M_acc=500000.0;configs[i].M_gyro=0.007;
			configs[i].base_speed=80;
			configs[i].sample_time=15;
			configs[i].fpt_limit=7;
			configs[i].kalibrierungsanzahl=250;
	i++;//lel nouverau
		sprintf(configs[i].configname,"Attitude noi");
			configs[i].Pa=2.7;configs[i].Da=0.3;configs[i].Ia=0.2;
			configs[i].Py=1.00;configs[i].Dy=0.2;configs[i].Iy=0.0;
			configs[i].P_acc=3.0;configs[i].P_gyro=1.0;configs[i].M_acc=650000.0;configs[i].M_gyro=0.001;//TODO 	1;
			configs[i].base_speed=80;
			configs[i].sample_time=15;
			configs[i].fpt_limit=7;
			configs[i].kalibrierungsanzahl=250;
	i++;//lel
		sprintf(configs[i].configname,"Attitude krass");
			configs[i].Pa=2.5;configs[i].Da=0.4;configs[i].Ia=0.4;
			configs[i].Py=1.00;configs[i].Dy=0.2;configs[i].Iy=0.0;
			configs[i].P_acc=3.0;configs[i].P_gyro=1.0;configs[i].M_acc=100000.0;configs[i].M_gyro=0.01;//TODO 	1;
			configs[i].base_speed=130;
			configs[i].sample_time=15;
			configs[i].fpt_limit=7;
			configs[i].kalibrierungsanzahl=250;


}
int select_config(int i,base_config* b){
	if(i<0)return -1;
	if(i>=configs_num)return -1;
	sprintf(b->configname,configs[i].configname);
	b->Pa=configs[i].Pa;	b->Da=configs[i].Da;	b->Ia=configs[i].Ia;
	b->Py=configs[i].Py;	b->Dy=configs[i].Dy;	b->Iy=configs[i].Iy;
	b->P_acc=configs[i].P_acc;	b->P_gyro=configs[i].P_gyro;	b->M_acc=configs[i].M_acc;	b->M_gyro=configs[i].M_gyro;
	b->base_speed=configs[i].base_speed;
	b->sample_time=configs[i].sample_time;
	b->fpt_limit=configs[i].fpt_limit;
	b->kalibrierungsanzahl=configs[i].kalibrierungsanzahl;
	return 1;
}
void ResetPBs(){PB[0]=0;PB[1]=0;PB[2]=0;}

void initConfig(base_config* c){
	assign_configs();
	//TODO kleiner auswahl assistenten hier einfuegen
	Config_Assitent(c);
	//select_config(default_one,c);//laedt grad eh standard config
	select_config(4,c);//laedt grad eh standard config

}
/**
 * ab hier ist noch nichts in benutzung, wird noch gemacht, dass man die p,i,d werte �ber usart einstellen kann
 */
void Config_Assitent(base_config* c){
	//Buttons auf setter verhalten umstellen
	PB_b[0]=0;	PB_b[1]=0;	PB_b[2]=0;
	dip204_clear_display();
	ResetPBs();
	sprintf(dip_mesg,"PB 1 uart conf");
	dip_write_string(dip_mesg,1);
	sprintf(dip_mesg,"PB 2 code conf");
	dip_write_string(dip_mesg,2);
	sprintf(dip_mesg,"PB 0 defa conf");
	dip_write_string(dip_mesg,3);
	sprintf(dip_mesg,"Loading Default in ");
	dip_write_string(dip_mesg,4);
	dip204_set_cursor_position(3,19);
	int tc_timeout=tc_ticks+5000;//5 sekunden
	int diff=0;
	ResetPBs();
	while(tc_ticks<tc_timeout&&!PB[1]&&!PB[2]&&!PB[0]){
		diff=tc_timeout-tc_ticks;
		diff/=1000;
		sprintf(dip_mesg,"Loading Default in %d",diff);
		dip_write_string(dip_mesg,4);
		wait_ms(250);//bissl warten, refresh rate gibts ja noch nich^^
	}
	if(PB[0]){//default
		ResetPBs();
		sprintf(dip_mesg,"LoadingDefault     ");
		dip_write_string(dip_mesg,4);
		select_config(default_one,c);
	}
	if(PB[1]){//werte empfang via usart
		ResetPBs();
		sprintf(dip_mesg,"Preparing:UART");
		dip_write_string(dip_mesg,4);
		Config_via_UART(c);
	}
	else if(PB[2]){//ausawehlen aus code
		ResetPBs();
		sprintf(dip_mesg,"Preparing:CODE");
		dip_write_string(dip_mesg,4);
		Config_via_CODE(c);
	}
	else{
		ResetPBs();
		sprintf(dip_mesg,"LoadingDefault");
		dip_write_string(dip_mesg,4);
		select_config(default_one,c);
	}
}
void Config_via_UART(base_config* c){
	dip204_clear_display();
	sprintf(dip_mesg,"UART RECEIVE ON");
	dip_write_string(dip_mesg,1);USART_schreiben(dip_mesg);
	int tc_timeout=tc_ticks+10000;//10 sekunden
	while(tc_ticks<tc_timeout){
		if(ringpuffer_A.r_pos!=ringpuffer_A.w_pos){//auslesen
			//TODO ebnf fuer config empfang
		}
	}

}
void Config_via_CODE(base_config* c){
	//TODO hier das auch mal machen , ggf am sonntag oder montag, iwas muss ja ma
}
