#define puffer_size 80
#include "structs.h"
#include "basics.h"
#include "AVR_Framework.h"
//char ring_puffer[puffer_size+10];
ringpuffer ringpuffer_A;

/**
 * schreibt in einen ringpuffer
 */
void insert_in_rbuffer(char c,ringpuffer* r){//traegt immer weiter ein, hoffentlich laueft er nich ueber
	r->mem[r->w_pos]=c;
	r->w_pos++;
}
int isAvailable(ringpuffer* r){
	if(r->r_pos==r->w_pos)return 0;
	else return 1;
}
/**
 * liest aus einen ringpuffer
 */
char read_from_rbuffer(ringpuffer* r){
	char erg=r->mem[r->r_pos];
	r->r_pos++;
	return erg;
}
/**
 * liest ausn standard ringpuffer
 */
char read_from_buffer(){
return read_from_rbuffer(&ringpuffer_A);
}
void Rpuffer_toDISP_raw(ringpuffer* A){
	char puffer[80];
	dip204_clear_display();
	sprintf(puffer,"RP%c::R_pos %d",A->name,A->r_pos);
		dip_write_string(puffer,1);
	sprintf(puffer,"RP%c::W_pos %d",A->name,A->w_pos);
		dip_write_string(puffer,2);
	sprintf(puffer,"RP:::RPPA %d",RPPA);
			dip_write_string(puffer,2);
	sprintf(puffer,"RP%c::VIEW",A->name);
		dip_write_string(puffer,4);
}
void clearRpuffer(ringpuffer* r){
	int i=0;
	r->r_pos=0;
	r->w_pos=0;
	for(i=0;i<RPPA;i++){
		r->mem[i]=0;
	}
	//kann natuerlich trotzdem passieren dass waehrend des loeschens n interrupt kommt und sagt, nope ich schreib da etz nei
}
