/*
 * enginCon.h
 *
 *  Created on: May 14, 2014
 *      Author: I8L-PC05-G01
 */
#include "structs.h"
#ifndef ENGINCON_H_
#define ENGINCON_H_


#endif /* ENGINCON_H_ */

void attitude_control(Engines* engin,regelungspara* param,int sampletime);
void yaw_control(Engines* engin,regelungspara* param,int sampletime);
void dip_view_engines(Engines* en);
void setEngine(Engines* en);

void regelung_angle_to_disp(regelungspara* param);
int isInStableFlight();
void setTrim(regelungspara* para);
