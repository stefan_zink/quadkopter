/*
 * AVR_Framework.h
 *
 *  Created on: 09.04.2014
 *      Author: Gageik
 */

#ifndef AVR_FRAMEWORK_H_
#define AVR_FRAMEWORK_H_

// HW Includes (Framework)
#include <avr32/io.h>
#include "board.h"
#include "compiler.h"
#include "dip204.h"
#include "intc.h"
#include "gpio.h"
#include "pm.h"
#include "pwm.h"
#include "power_clocks_lib.h"
#include "pwm.h"
#include "tc.h"
#include "delay.h"
#include "spi.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>


#endif /* AVR_FRAMEWORK_H_ */
