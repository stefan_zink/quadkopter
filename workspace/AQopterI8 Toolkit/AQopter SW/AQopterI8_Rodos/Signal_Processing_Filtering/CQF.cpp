/*
 * Complementary Kalman Filter
 *
 *
 * AQopterI8 Software Framekwork
 *
 * This Quadcopter Software Framework was developed at
 * University Wuerzburg
 * Chair Computer Science 8
 * Aerospace Information Technology
 * Copyright (c) 2011-2014 Nils Gageik
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Translation/Portation to C++ on RODOS: SM/MR/NG, 2014
 * */

#include "rodos.h"
#include "activity.h"

#include "AQopterI8.h"

// latter to be transmitted using mitddleware !!
extern SignalProcessed SP_Data;

/******* Controller  ************************************/

namespace CQF_Filtering {

class CQF : public Activity {
public:
    CQF() : Activity("CQF", 450, FILTER_START_TIME*SECONDS, SAMPLE_TIME*MILLISECONDS) { }
    void step(int64_t timeNow);

    /*****************************************/
};

#ifdef NEW_ORIENTATION
CQF cqf;
#endif
/********************************************************/

void CQF::step(int64_t timeNow) {

	#ifdef DEBUG_THREAD_LETTERS
	xprintf("q");
	#endif

    SignalProcessed Out;

    // Correct Quaternion using Acc & Mag Vector (Measurements)
    correctQuat2();

    // Calculate RPY Angles from Current Orientation Quaternion for Control
    calcRPY();

    Out.roll_angle = sensorWerte.roll_angle;
    Out.pitch_angle = sensorWerte.pitch_angle;
    Out.yaw_angle = sensorWerte.yaw_angle;

    Out.roll_delta_angle = sensorWerte.roll_delta_angle;
    Out.pitch_delta_angle = sensorWerte.pitch_delta_angle;
    Out.yaw_delta_angle = sensorWerte.yaw_delta_angle;

    SP_Data =  Out;

	#ifdef DEBUG_THREAD_LETTERS
	xprintf("Q");
	#endif
}


} // namespace


