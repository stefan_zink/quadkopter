/*
 * Signal Processing
 *
 * AQopterI8 Software Framekwork
 *
 * This Quadcopter Software Framework was developed at
 * University Wuerzburg
 * Chair Computer Science 8
 * Aerospace Information Technology
 * Copyright (c) 2011-2014 Nils Gageik
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Translation/Portation to C++ on RODOS: SM/MR/NG, 2014
 * */

#include "rodos.h"
#include "activity.h"

#include "AQopterI8.h"

// latter to be transmitted using mitddleware !!
extern SignalProcessed SP_Data;

extern "C"{
    #include "QUAD_TWI_6DOF.h"
}

extern unsigned int tc_ticks;

/******* Controller  ************************************/

namespace SignalProcessing {

class SignalProcesor : public Activity {
public:
    SignalProcesor() : Activity("SignalProcesor", 500, SIGNAL_PROCESSING_START_TIME*SECONDS, SAMPLE_TIME*MILLISECONDS) { } // Warnins: Sensos need higher (>200) priority
    void init() { calibrate(); }
    void step(int64_t timeNow);
    void calibrate();
    void sensoring();
    void conditioning();
    int64_t last_Time;

}signalProcesor;

/********************************************************/

void SignalProcesor::step(int64_t timeNow) {

	#ifdef DEBUG_THREAD_LETTERS
    xprintf("s");
	#endif

    SignalProcessed Out;

	#ifdef DEBUG_TIMING_DIFFERENCE
    int64_t diff_time = timeNow - last_Time;
    xprintf("	Time: %d\n", (int)diff_time);
    last_Time = timeNow;
	#endif

    // Get Values from Sensors and Calibrate
    sensoring();
    conditioning();

	#ifdef DEBUG_THREAD_LETTERS
	xprintf("�");
	#endif

    // Rotate Current Quaternion according to new Gyro Values
    Quat = rotateQuat(Quat, sensorWerte.gyro_x_sampletimed, sensorWerte.gyro_y_sampletimed, sensorWerte.gyro_z_sampletimed);

    Out.roll_angle  = sensorWerte.roll_angle;
    Out.pitch_angle = sensorWerte.pitch_angle;
    Out.yaw_angle   = sensorWerte.yaw_angle;

    Out.roll_delta_angle  = sensorWerte.gyro_y;
    Out.pitch_delta_angle = sensorWerte.gyro_x;
    Out.yaw_delta_angle   = sensorWerte.gyro_z;

    SP_Data =  Out;

	#ifdef DEBUG_THREAD_LETTERS
    xprintf("S");
	#endif
}


/*************************************************************/
void SignalProcesor::calibrate() {
    xprintf("calibration\n");
    int64_t startTime = NOW();

    // Calculate Bias with Gravitation Compensation

    // ---- Calibration ----------------------------
    // Calibrate Sensor

    sensorDaten_raw neuerWert;
    int error = 0;

    double z_Ebene_Vector[3];

    sensorWerte.acc_x_bias = 0.0f;
    sensorWerte.acc_y_bias = 0.0f;
    sensorWerte.acc_z_bias = 0.0f;

    sensorWerte.gyro_x_bias = 0.0f;
    sensorWerte.gyro_y_bias = 0.0f;
    sensorWerte.gyro_z_bias = 0.0f;

    controllerWerte.fehlerintegral_roll 	= 0.0f;
    controllerWerte.fehlerintegral_pitch 	= 0.0f;
    controllerWerte.fehlerintegral_yaw 		= 0.0f;
    sensorWerte.winkel_x_gyro = 0.0f;
    sensorWerte.winkel_y_gyro = 0.0f;
    sensorWerte.winkel_z_gyro = 0.0f;

    // Read Fix Calibrated Values
    sensorWerte.acc_sensivity_x = 1000.0;
    sensorWerte.acc_sensivity_y = 1000.0;
    sensorWerte.acc_sensivity_z = 1000.0;

    sensorWerte.mag_sensivity_x = 250.0;
    sensorWerte.mag_sensivity_y = 250.0;
    sensorWerte.mag_sensivity_z = 250.0;


#ifdef CALIBRATION_DEBUG
    xprintf("	Acc Sensivity (x/y/z): %3.4f/%3.4f/%3.4f\n", sensorWerte.acc_sensivity_x, sensorWerte.acc_sensivity_y, sensorWerte.acc_sensivity_z);
    xprintf("	Mag Sensivity (x/y/z): %3.4f/%3.4f/%3.4f\n", sensorWerte.mag_sensivity_x, sensorWerte.mag_sensivity_y, sensorWerte.mag_sensivity_z);
    xprintf("	Acc Bias (x/y/z): %3.4f/%3.4f/%3.4f\n", sensorWerte.acc_x_bias, sensorWerte.acc_y_bias, sensorWerte.acc_z_bias);
    xprintf("	Mag Bias (x/y/z): %3.4f/%3.4f/%3.4f\n", sensorWerte.mag_x_bias, sensorWerte.mag_y_bias, sensorWerte.mag_z_bias);
#endif

    xprintf("	Start Sensor\n");
    xprintf("	waiting until %3.6f for Calibration\n", (double)(startTime + 1000*MILLISECONDS)/(double)SECONDS);
    BUSY_WAITING_UNTIL(startTime + 1000*MILLISECONDS);
    gpio_set_gpio_pin(SENSOR_CALIBRATE_LED);

    xprintf("	Calibration-loop\n");

    int64_t timeStep = NOW();
    for (int i = 0; i < BIAS_SAMPLES; i++) {

        error = read_ex_IMU_Sensors(&neuerWert);
        cond_IMU_Sensors(&neuerWert);

        if (error) {
            steuerWerte.regler_on = 0;
            xprintf("	ERR! TWI Calibration Error\n");
            gpio_clr_gpio_pin(SENSOR_CALIBRATE_LED);
            return;
        }

#ifdef ACC_DYNAMIC_BIAS
        sensorWerte.acc_x_bias = sensorWerte.acc_x_bias + neuerWert.acc_x_f;
        sensorWerte.acc_y_bias = sensorWerte.acc_y_bias + neuerWert.acc_y_f;
        sensorWerte.acc_z_bias = sensorWerte.acc_z_bias + neuerWert.acc_z_f;
#endif

        sensorWerte.gyro_x_bias = sensorWerte.gyro_x_bias + neuerWert.gyro_x_f;
        sensorWerte.gyro_y_bias = sensorWerte.gyro_y_bias + neuerWert.gyro_y_f;
        sensorWerte.gyro_z_bias = sensorWerte.gyro_z_bias + neuerWert.gyro_z_f;

        sensorWerte.Mag_Vec_Init[0] = sensorWerte.Mag_Vec_Init[0] + neuerWert.mag_x_f;
        sensorWerte.Mag_Vec_Init[1] = sensorWerte.Mag_Vec_Init[1] + neuerWert.mag_y_f;
        sensorWerte.Mag_Vec_Init[2] = sensorWerte.Mag_Vec_Init[2] + neuerWert.mag_z_f;


#ifdef DISPLAY_ON
        if (i == BIAS_SAMPLES / 100) xprintf("	Att. Contr. 0,01");
        if (i == BIAS_SAMPLES /  10) xprintf("	Att. Contr. 0,10");
        if (i == BIAS_SAMPLES /   2) xprintf("	Att. Contr. 0,50");
#endif

        if (i == BIAS_SAMPLES / 2) gpio_clr_gpio_pin(SENSOR_CALIBRATE_LED);

        // Messungen im 1ms takt
        timeStep += 1*MILLISECONDS;
        BUSY_WAITING_UNTIL(timeStep);
    }

    xprintf("	Calibration Finished!\n");

    gpio_set_gpio_pin(SENSOR_CALIBRATE_LED);

    // Init Mag Vectors considering Bias and Sensivity
    sensorWerte.Mag_Vec_Init[0] = (sensorWerte.Mag_Vec_Init[0] / (double)BIAS_SAMPLES - sensorWerte.mag_x_bias)
                                  / sensorWerte.mag_sensivity_x;
    sensorWerte.Mag_Vec_Init[1] = (sensorWerte.Mag_Vec_Init[1] / (double)BIAS_SAMPLES - sensorWerte.mag_y_bias)
                                  / sensorWerte.mag_sensivity_y;
    sensorWerte.Mag_Vec_Init[2] = (sensorWerte.Mag_Vec_Init[2] / (double)BIAS_SAMPLES - sensorWerte.mag_z_bias)
                                  / sensorWerte.mag_sensivity_z;

    normalize_vector(sensorWerte.Mag_Vec_Init);

    // Compute Mag Initial Quaternion to transform Mag_Vector into z-axis
    z_Ebene_Vector[0] = sensorWerte.Mag_Vec_Init[0];
    z_Ebene_Vector[1] = sensorWerte.Mag_Vec_Init[1];
    z_Ebene_Vector[2] = 0.0;

    Mag_Quat_Init = Quat_from_Vectors(z_Ebene_Vector,sensorWerte.Mag_Vec_Init);

    // Rotate Mag_Vector_Init into z-plane
    Q_rotate_Vec(sensorWerte.Mag_Vec_Init, Mag_Quat_Init, sensorWerte.Mag_Vec_Init);

    sensorWerte.acc_x_bias = sensorWerte.acc_x_bias / (double)BIAS_SAMPLES;
    sensorWerte.acc_y_bias = sensorWerte.acc_y_bias / (double)BIAS_SAMPLES;
    sensorWerte.acc_z_bias = sensorWerte.acc_z_bias / (double)BIAS_SAMPLES + sensorWerte.acc_sensivity_z;

    // Init Acc Vector
    sensorWerte.Acc_Vec_Init[0] = 0.0;
    sensorWerte.Acc_Vec_Init[1] = 0.0;
    sensorWerte.Acc_Vec_Init[2] = -1.0;

    sensorWerte.gyro_x_bias = sensorWerte.gyro_x_bias / (double)BIAS_SAMPLES;
    sensorWerte.gyro_y_bias = sensorWerte.gyro_y_bias / (double)BIAS_SAMPLES;
    sensorWerte.gyro_z_bias = sensorWerte.gyro_z_bias / (double)BIAS_SAMPLES;

    // Init Quaternion
    Quat.q0 = 1.0;
    Quat.q1 = 0.0;
    Quat.q2 = 0.0;
    Quat.q3 = 0.0;

    //Init SP Variables
    sensorWerte.gyro_x = 0.0f;
    sensorWerte.gyro_y = 0.0f;
    sensorWerte.gyro_z = 0.0f;

    sensorWerte.RohWerte.acc_x_s = 0;
    sensorWerte.RohWerte.acc_y_s = 0;
    sensorWerte.RohWerte.acc_z_s = 0;
    sensorWerte.RohWerte.gyro_x_s = 0;
    sensorWerte.RohWerte.gyro_y_s = 0;
    sensorWerte.RohWerte.gyro_z_s = 0;
    sensorWerte.RohWerte.mag_x_s = 0;
    sensorWerte.RohWerte.mag_y_s = 0;
    sensorWerte.RohWerte.mag_z_s = 0;

    // Init Feuersuche-Temp Values
    //sensorWerte.temp_max_winkel = 0;
    //sensorWerte.temp_max_aussen = 10;

    xprintf("	Duration of Calibration: %3.6f\n", (double)(NOW() - startTime)/(double)SECONDS);
}


void SignalProcesor::sensoring() {
    // Get SensorValues, Bias, Integrate and Process

    sensorDaten_raw sensorWerte_raw1, sensorWerte_raw2;

	#ifdef DEBUG_TIMING_1
    xprintf("%d\n", tc_ticks);
	#endif

    // **********  I M U ***************************************************************************************************
    // IMU Readings for Attitude
    // Read Twice to overcome data corruption coz of bus errors on communication line
    read_IMU_Sensors(&sensorWerte_raw1);
    read_IMU_Sensors(&sensorWerte_raw2);

    sensorWerte.RohWerte.acc_x_s = myMedian(sensorWerte_raw1.acc_x_s, sensorWerte_raw2.acc_x_s, sensorWerte.RohWerte.acc_x_s);
    sensorWerte.RohWerte.acc_y_s = myMedian(sensorWerte_raw1.acc_y_s, sensorWerte_raw2.acc_y_s, sensorWerte.RohWerte.acc_y_s);
    sensorWerte.RohWerte.acc_z_s = myMedian(sensorWerte_raw1.acc_z_s, sensorWerte_raw2.acc_z_s, sensorWerte.RohWerte.acc_z_s);

#ifndef GYRO_AUSREISSER_FILTER
    sensorWerte.RohWerte.gyro_x_s = myMedian(sensorWerte_raw1.gyro_x_s, sensorWerte_raw2.gyro_x_s, sensorWerte.RohWerte.gyro_x_s);
    sensorWerte.RohWerte.gyro_y_s = myMedian(sensorWerte_raw1.gyro_y_s, sensorWerte_raw2.gyro_y_s, sensorWerte.RohWerte.gyro_y_s);
    sensorWerte.RohWerte.gyro_z_s = myMedian(sensorWerte_raw1.gyro_z_s, sensorWerte_raw2.gyro_z_s, sensorWerte.RohWerte.gyro_z_s);
#else
    sensorWerte.RohWerte.gyro_x_s = Calc_New_AusreisserFilter(&Gyro_X_Ausreisser_Filter, sensorWerte_raw1.gyro_x_s, sensorWerte_raw2.gyro_x_s);
    sensorWerte.RohWerte.gyro_y_s = Calc_New_AusreisserFilter(&Gyro_Y_Ausreisser_Filter, sensorWerte_raw1.gyro_y_s, sensorWerte_raw2.gyro_y_s);
    sensorWerte.RohWerte.gyro_z_s = Calc_New_AusreisserFilter(&Gyro_Z_Ausreisser_Filter, sensorWerte_raw1.gyro_z_s, sensorWerte_raw2.gyro_z_s);
#endif

    sensorWerte.RohWerte.mag_x_s = myMedian(sensorWerte_raw1.mag_x_s, sensorWerte_raw2.mag_x_s, sensorWerte.RohWerte.mag_x_s);
    sensorWerte.RohWerte.mag_y_s = myMedian(sensorWerte_raw1.mag_y_s, sensorWerte_raw2.mag_y_s, sensorWerte.RohWerte.mag_y_s);
    sensorWerte.RohWerte.mag_z_s = myMedian(sensorWerte_raw1.mag_z_s, sensorWerte_raw2.mag_z_s, sensorWerte.RohWerte.mag_z_s);
}



void SignalProcesor::conditioning() {
// Conditions the Sensor Values: Coming from Raw Values to calibrated, mean filtered values

    // Sensor Condition of Each Sensor
    cond_IMU_Sensors(&sensorWerte.RohWerte);

    // Bias-Compensation
    sensorWerte.acc_x = (sensorWerte.RohWerte.acc_x_f - sensorWerte.acc_x_bias) * 1000 / sensorWerte.acc_sensivity_x;
    sensorWerte.acc_y = (sensorWerte.RohWerte.acc_y_f - sensorWerte.acc_y_bias) * 1000 / sensorWerte.acc_sensivity_y;
    sensorWerte.acc_z = (sensorWerte.RohWerte.acc_z_f - sensorWerte.acc_z_bias) * 1000 / sensorWerte.acc_sensivity_z;

    sensorWerte.gyro_x = sensorWerte.RohWerte.gyro_x_f - sensorWerte.gyro_x_bias;
    sensorWerte.gyro_y = sensorWerte.RohWerte.gyro_y_f - sensorWerte.gyro_y_bias;
    sensorWerte.gyro_z = sensorWerte.RohWerte.gyro_z_f - sensorWerte.gyro_z_bias;

    sensorWerte.mag_x = (sensorWerte.RohWerte.mag_x_f - sensorWerte.mag_x_bias) * 1000 / sensorWerte.mag_sensivity_x;
    sensorWerte.mag_y = (sensorWerte.RohWerte.mag_y_f - sensorWerte.mag_y_bias) * 1000 / sensorWerte.mag_sensivity_y;
    sensorWerte.mag_z = (sensorWerte.RohWerte.mag_z_f - sensorWerte.mag_z_bias) * 1000 / sensorWerte.mag_sensivity_z;

    sensorWerte.mag_magnitude = sqrt(sensorWerte.mag_x * sensorWerte.mag_x
                                     + sensorWerte.mag_y * sensorWerte.mag_y
                                     + sensorWerte.mag_z * sensorWerte.mag_z);

    /*
    sensorWerte.mag_magnitude = sqrt(sensorWerte.RohWerte.mag_x_f * sensorWerte.RohWerte.mag_x_f
    		+ sensorWerte.RohWerte.mag_y_f * sensorWerte.RohWerte.mag_y_f
    		+ sensorWerte.RohWerte.mag_z_f * sensorWerte.RohWerte.mag_z_f);
    		*/

    fill_vector(sensorWerte.mag_x, sensorWerte.mag_y, sensorWerte.mag_z, sensorWerte.Mag_Vec);
    fill_vector(sensorWerte.acc_x, sensorWerte.acc_y, sensorWerte.acc_z, sensorWerte.Acc_Vec);

    normalize_vector(sensorWerte.Mag_Vec);
    normalize_vector(sensorWerte.Acc_Vec);

    // Digital Lowpass Filter
    sensorWerte.acc_x_dpf = (double)(iir1((float)sensorWerte.acc_x));
    sensorWerte.acc_y_dpf = (double)(iir2((float)sensorWerte.acc_y));

    // Convert Gyro Values according to SAMPLE_TIME (ms)
    sensorWerte.gyro_x_sampletimed = sensorWerte.gyro_x * SAMPLE_TIME / 1000.f;
    sensorWerte.gyro_y_sampletimed = sensorWerte.gyro_y * SAMPLE_TIME / 1000.f;
    sensorWerte.gyro_z_sampletimed = sensorWerte.gyro_z * SAMPLE_TIME / 1000.f;

    sensorWerte.winkel_x_gyro = sensorWerte.winkel_x_gyro + sensorWerte.gyro_x_sampletimed;
    sensorWerte.winkel_y_gyro = sensorWerte.winkel_y_gyro + sensorWerte.gyro_y_sampletimed;
    sensorWerte.winkel_z_gyro = sensorWerte.winkel_z_gyro + sensorWerte.gyro_z_sampletimed;

    // For KalmanVarianz Computation: Calc Two Means: Current, and delayed (by Filter_size shifted)
    Calc_New_Mean(&MF_Acc_X_Mean_Delayed, Calc_New_Mean(&MF_Acc_X_Mean, sensorWerte.acc_x_dpf));
    Calc_New_Mean(&MF_Acc_Y_Mean_Delayed, Calc_New_Mean(&MF_Acc_Y_Mean, sensorWerte.acc_y_dpf));
    Calc_New_Mean(&MF_Acc_Z_Mean_Delayed, Calc_New_Mean(&MF_Acc_Z_Mean, sensorWerte.acc_z));

    // Saturate, coz asin needs -1,+1
    sensorWerte.acc_x = my_saturate(sensorWerte.acc_x_dpf, sensorWerte.acc_sensivity_x);
    sensorWerte.acc_y = my_saturate(MF_Acc_Y_Mean.value, sensorWerte.acc_sensivity_y);

    sensorWerte.roll_acc_old = sensorWerte.roll_acc;
    sensorWerte.pitch_acc_old = sensorWerte.pitch_acc;

    sensorWerte.roll_acc =  asin (sensorWerte.acc_y / 1000) * -180.0f / PI;
    sensorWerte.pitch_acc =  asin (sensorWerte.acc_x / 1000) * 180.0f / PI;
}






} // namespace


