/*
 * basics.h
 *
 *  Created on: 12.04.2011
 *      Author: gageik
 */

#pragma once

#include "basics.h"
#include "myMath.h"
#include "Quad_Data.h"


#ifdef	COMP_MODE_VERY_FAST
#define COMPENSATION_CUT_OFF 			0.002
#else
#ifdef		COMP_MODE_FAST
#define COMPENSATION_CUT_OFF 			0.0015
#else
#define COMPENSATION_CUT_OFF 			0.001
#endif
#endif

void setRPY(double roll, double pitch, double yaw);

Quaternion rotateQuat(Quaternion myQuat, double x, double y, double z);
Quaternion rotateQuat2(Quaternion myQuat, double x, double y, double z);
Quaternion calcQuatfromFW(double x, double y, double z);
void calcRPY(void);
void calcRPY2(Quaternion myQuat, double* RPY);
void calcRPY_SM(void);
Quaternion calcQuat(double x, double y, double z);
void correctQuat(double x, double y, double z);
void correctQuat2();
void Conditioning();

Quaternion compensate_Drift(Quaternion Quat_Acc, Quaternion Quat_Mag);

void Kalman_Set_RP(double roll, double pitch);



/*
 * x -> Roll Axis (For Gyro and Acc)
 * y -> Pitch Axis (For Gyro and Acc)
 * Acc and Gyro use different reference system: x and y are switched so Acc_x is Angle around x-axis
 */

// Common with signal-processing

extern Quaternion Quat;           // Orientation Quaternion
extern Quaternion Acc_Quat;
extern Quaternion Mag_Quat;
extern Quaternion Mag_Quat_Init; // Quaternion, das den Init_Mag_Sensor in die z-Ebene rotiert


