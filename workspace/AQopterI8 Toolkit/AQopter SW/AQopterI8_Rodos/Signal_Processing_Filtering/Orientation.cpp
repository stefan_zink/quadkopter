/*
 *  Orientation.cpp
 *
 *  Created on: 12.04.2011
 *      Author: gageik
 *
 * AQopterI8 Software Framekwork
 *
 * This Quadcopter Software Framework was developed at
 * University Wuerzburg
 * Chair Computer Science 8
 * Aerospace Information Technology
 * Copyright (c) 2011-2014 Nils Gageik
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Translation/Portation to C++ on RODOS: SM/MR/NG, 2014
 * */

#include "rodos.h" // only for xprintf

#include "AQopterI8.h"

Quaternion Quat;           // Orientation Quaternion
Quaternion Acc_Quat;
Quaternion Mag_Quat;
Quaternion Mag_Quat_Init; // Quaternion, das den Init_Mag_Sensor in die z-Ebene rotiert
double RPY_Acc[3];
double RPY_Mag[3];

double Acc_Vec_Mean[3] = {0.0, 0.0, 0.0};
double Acc_Trans_Mean[3] = {0.0, 0.0, 0.0};

#ifdef	COMP_MODE_VERY_FAST
#define QUAT_CORRECT_INTERVALL		100		// In Factors of SAMPLE_TIME: 200 was good
#else
#ifdef		COMP_MODE_FAST
#define QUAT_CORRECT_INTERVALL		200		// In Factors of SAMPLE_TIME: 200 was good

#else
#define QUAT_CORRECT_INTERVALL		500		// In Factors of SAMPLE_TIME: 200 was good
#endif
#endif


Quaternion rotateQuat2(Quaternion myQuat, double x, double y, double z) {
    // Rotate Quaternion According to three Angles x,y,z
    // Atheel Version
    double norm;          		  // vector norm
    double q0_D, q1_D, q2_D, q3_D; // quaternion derrivative from gyroscopes elements

    double gx_rad_h=x*PI/360;
    double gy_rad_h=y*PI/360;
    double gz_rad_h=z*PI/360;


    // Compute the quaternion derrivative measured by gyroscopes
    q0_D =-myQuat.q1 * gx_rad_h - myQuat.q2 * gy_rad_h - myQuat.q3 * gz_rad_h;
    q1_D = myQuat.q0 * gx_rad_h + myQuat.q2 * gz_rad_h - myQuat.q3 * gy_rad_h;
    q2_D = myQuat.q0 * gy_rad_h - myQuat.q1 * gz_rad_h + myQuat.q3 * gx_rad_h;
    q3_D = myQuat.q0 * gz_rad_h + myQuat.q1 * gy_rad_h - myQuat.q2 * gx_rad_h;

    // integrate the estimated quaternion derrivative
    myQuat.q0 += q0_D;
    myQuat.q1 += q1_D;
    myQuat.q2 += q2_D;
    myQuat.q3 += q3_D;

    // Normalise quaternion
    norm = sqrt(myQuat.q0*myQuat.q0 + myQuat.q1*myQuat.q1 + myQuat.q2*myQuat.q2 + myQuat.q3*myQuat.q3);
    myQuat.q0 /= norm;
    myQuat.q1 /= norm;
    myQuat.q2 /= norm;
    myQuat.q3 /= norm;

    return myQuat;
}


Quaternion rotateQuat(Quaternion myQuat, double x, double y, double z) {
    // Calculate Quaternion from RPY
    // from Book Kuipers

    Quaternion dQuat, rQ;

    double betrag;

    double cdx = cos(x* (double)PI / 360.0f);
    double sdx = sin(x* (double)PI / 360.0f);
    double cdy = cos(y* (double)PI / 360.0f);
    double sdy = sin(y* (double)PI / 360.0f);
    double cdz = cos(z* (double)PI / 360.0f);
    double sdz = sin(z* (double)PI / 360.0f);

    //% Transforms RPY EulerAngles into Quaternion
    //% dx angle of rotation about x-axis (using RAD) | cdx = cos(dx) etc.
    //% dy angle of rotation about y-axis
    //% dz angle of rotation about z-axis

    // Compute dq
    dQuat.q0 = cdz*cdy*cdx + sdz*sdy*sdx;
    dQuat.q1 = cdz*cdy*sdx - sdz*sdy*cdx;
    dQuat.q2 = cdz*sdy*cdx + sdz*cdy*sdx;
    dQuat.q3 = sdz*cdy*cdx - cdz*sdy*sdx;

    betrag = sqrt(dQuat.q0*dQuat.q0+dQuat.q1*dQuat.q1+dQuat.q2*dQuat.q2+dQuat.q3*dQuat.q3);
    dQuat.q0 = dQuat.q0 / betrag;
    dQuat.q1 = dQuat.q1 / betrag;
    dQuat.q2 = dQuat.q2 / betrag;
    dQuat.q3 = dQuat.q3 / betrag;

    // Update Orientation-Quaternion: Quaternionenprodukt:
    // rQ = myQuat x dQuat
    rQ = Quatprodukt(myQuat, dQuat);

    //Normalise
    betrag = sqrt(rQ.q0*rQ.q0+rQ.q1*rQ.q1+rQ.q2*rQ.q2+rQ.q3*rQ.q3);
    rQ.q0 = rQ.q0 / betrag;
    rQ.q1 = rQ.q1 / betrag;
    rQ.q2 = rQ.q2 / betrag;
    rQ.q3 = rQ.q3 / betrag;

    return rQ;
}


void correctQuat(double x, double y, double z) {
    // Correct the Quaternion usign RPY from Kalmanfilter
    // Does not correct every cycle to overcome instability & noise feedback

    //double old_x, old_y, old_z;		// Current, drifted, old values
    //double delta_x_old, delta_y_old;
    //double delta_x, delta_y;

    sensorWerte.kalman_correct_index++;


    if (sensorWerte.kalman_correct_index == QUAT_CORRECT_INTERVALL) {

        // Save current, drifted values
        //old_x = sensorWerte.roll_angle;
        //old_y = sensorWerte.pitch_angle;
        //old_z = sensorWerte.yaw_angle;

        //sendDebugValues(Quat.q0,Quat.q1,Quat.q2,Quat.q3,0,0);
        Quat = calcQuat(x, y, z);
        calcRPY();		// Renew RPY Angles for correct delta computation

        //delta_x_old = sensorWerte.roll_angle_old - sensorWerte.roll_angle;
        //delta_y_old = sensorWerte.pitch_angle_old - sensorWerte.pitch_angle;
        //delta_z_old = sensorWerte.yaw_angle_old - sensorWerte.yaw_angle;

        //delta_x = sensorWerte.roll_angle - old_x;
        //delta_y = sensorWerte.pitch_angle - old_y;
        //delta_z = sensorWerte.yaw_angle - old_z;

        sensorWerte.kalman_correct_index = 0;
    }
}

void correctQuat2() {
    // New Correction Approach with Quaternion Vectors rotating back

	Quaternion compensation_Quat = {1.0, 0.0, 0.0, 0.0};

    double compensation_cut_on = (double)1.0 - (double)COMPENSATION_CUT_OFF;

    // Transform Initial Measurements according to current Orientation
    Q_rotate_Vec(sensorWerte.Acc_Vec_Init, Quat, sensorWerte.Acc_Vec_trans);

    // Rotate Mag Vector according to Orientation and transform in z-plane
    Q_rotate_Vec(sensorWerte.Mag_Vec, Q_konjugiert(Quat), sensorWerte.Mag_Vec_trans);
    Q_rotate_Vec(sensorWerte.Mag_Vec_trans, Mag_Quat_Init, sensorWerte.Mag_Vec_trans);

    Exponential_Vektor_Fusion(compensation_cut_on, COMPENSATION_CUT_OFF, Acc_Vec_Mean, sensorWerte.Acc_Vec);
    Exponential_Vektor_Fusion(compensation_cut_on, COMPENSATION_CUT_OFF, Acc_Trans_Mean, sensorWerte.Acc_Vec_trans);

    // Compute Quaternion from transformed current Vectors to measured Vectors for compensation
    Acc_Quat = Quat_from_Vectors(Acc_Vec_Mean, Acc_Trans_Mean);
    Mag_Quat = Quat_from_Vectors(sensorWerte.Mag_Vec_trans, sensorWerte.Mag_Vec_Init);

    // Compute RPY for debugging
    calcRPY2(Acc_Quat, RPY_Acc);
    calcRPY2(Mag_Quat, RPY_Mag);

    // Compute Compensation Quaternion
    compensation_Quat = compensate_Drift(Acc_Quat, Mag_Quat);

    //Transform (Update) Mean Filterd Vectors according to compensation Quaternion
    Q_rotate_Vec(Acc_Vec_Mean, Q_konjugiert(compensation_Quat), Acc_Vec_Mean);

    Quat = Quatprodukt(Quat, compensation_Quat);
}



Quaternion calcQuat(double x, double y, double z) {
    // Computes Quaternion from Euler Angles (RPY - y,x,z)
    double d;

    Quaternion Quat2;

    double cdx = cos(x* (double)PI / 360.0f);
    double sdx = sin(x* (double)PI / 360.0f);
    double cdy = cos(y* (double)PI / 360.0f);
    double sdy = sin(y* (double)PI / 360.0f);
    double cdz = cos(z* (double)PI / 360.0f);
    double sdz = sin(z* (double)PI / 360.0f);

    Quat2.q0 = (cdx*cdy*cdz+sdx*sdy*sdz);
    Quat2.q1 = (sdx*cdy*cdz-cdx*sdy*sdz);
    Quat2.q2 = (cdx*sdy*cdz+sdx*cdy*sdz);
    Quat2.q3 = (cdx*cdy*sdz-sdx*sdy*cdz);

    //Normalise
    d = sqrt(Quat2.q0*Quat2.q0+Quat2.q1*Quat2.q1+Quat2.q2*Quat2.q2+Quat2.q3*Quat2.q3);
    Quat2.q0 = Quat2.q0 / d;
    Quat2.q1 = Quat2.q1 / d;
    Quat2.q2 = Quat2.q2 / d;
    Quat2.q3 = Quat2.q3 / d;

    return Quat2;
}



void calcRPY() {
    // Compute RPY Angular Rate (roll_angle & roll_delta_angle):
    // More accurate then using gyro x/y/z, In fact these are the transformed gyro x/y/z
    // Take them from Quat, write them to sensorWerte

    // RPY formular from Wiki
    double x,y,z;

    // Compute Roll/Pitch/Yaw Angle from Quat
    x = atan2( (2*(Quat.q0*Quat.q1 + Quat.q2*Quat.q3)), (1-2*(Quat.q1*Quat.q1+Quat.q2*Quat.q2)) ) * 180.0f / PI;

    y = asin( (2*(Quat.q0*Quat.q2 - Quat.q3*Quat.q1)) ) * 180.0f / PI;

    z = atan2( (2*(Quat.q0*Quat.q3 + Quat.q1*Quat.q2)), (1-2*(Quat.q2*Quat.q2+Quat.q3*Quat.q3)) ) * 180.0f / PI;


    // Compute Angular Rate with reference to actual orientation
    // -> Transformed Gyro Value
    sensorWerte.roll_delta_angle 	= (x - sensorWerte.roll_angle)	 * 1000 / SAMPLE_TIME;
    sensorWerte.pitch_delta_angle 	= (y - sensorWerte.pitch_angle)	 * 1000 / SAMPLE_TIME;
    sensorWerte.yaw_delta_angle 	= (z - sensorWerte.yaw_angle)	 * 1000 / SAMPLE_TIME;


    // Save last RPY angle for next step
    sensorWerte.roll_angle = x;
    sensorWerte.pitch_angle = y;
    sensorWerte.yaw_angle = z;
}

void calcRPY2(Quaternion myQuat, double* RPY) {
    // Compute RPY Angular Rate (roll_angle & roll_delta_angle) from a Quaternion, write to RPY 3x1 vector
    // RPY formular from Wiki

    // Compute Roll/Pitch/Yaw Angle from Quat
    RPY[0] = atan2( (2*(myQuat.q0*myQuat.q1 + myQuat.q2*myQuat.q3)), (1-2*(myQuat.q1*myQuat.q1+myQuat.q2*myQuat.q2)) ) * 180.0f / PI;

    RPY[1] = asin( (2*(myQuat.q0*myQuat.q2 - myQuat.q3*myQuat.q1)) ) * 180.0f / PI;

    RPY[2] = atan2( (2*(myQuat.q0*myQuat.q3 + myQuat.q1*myQuat.q2)), (1-2*(myQuat.q2*myQuat.q2+myQuat.q3*myQuat.q3)) ) * 180.0f / PI;
}

// Other implementation, not better then calcRPY()
void calcRPY_SM() {

    double x,y,z;

    double m21 = 2 * Quat.q1 * Quat.q2  + 2 * Quat.q0 * Quat.q3;
    double m11 = 2 * Quat.q0 * Quat.q0 - 1 + 2 * Quat.q1 * Quat.q1;
    double m31 = 2 * Quat.q1 * Quat.q3  - 2 * Quat.q0 * Quat.q2;
    double m32 = 2 * Quat.q2 * Quat.q3  + 2 * Quat.q0 *Quat.q1;
    double m33 = 2 * Quat.q0 * Quat.q0 - 1 + 2 * Quat.q3 * Quat.q3;

    x  = atan2(m32, m33) * 180.0f / PI;
    y = atan2(-m31, sqrt(m11*m11 + m21*m21)) * 180.0f / PI;
    z  = atan2(m21, m11) * 180.0f / PI;


    // Compute Angular Rate with reference to actual orientation
    // -> Transformed Gyro Value
    sensorWerte.roll_delta_angle 	= (x - sensorWerte.roll_angle)	 * 1000 / SAMPLE_TIME;
    sensorWerte.pitch_delta_angle 	= (y - sensorWerte.pitch_angle)	 * 1000 / SAMPLE_TIME;
    sensorWerte.yaw_delta_angle 	= (z - sensorWerte.yaw_angle)	 * 1000 / SAMPLE_TIME;


    // Save last RPY angle for next step
    sensorWerte.roll_angle = x;
    sensorWerte.pitch_angle = y;
    sensorWerte.yaw_angle = z;
}

Quaternion compensate_Drift(Quaternion Quat_Acc, Quaternion Quat_Mag) {
    // return Compensation Quaternion

	static char my_toogle = 1;

    Quaternion compensation_Quat = {1.0, 0.0, 0.0, 0.0};

	#define ACC_NOISE		1000
	#define MAG_NOISE		100

	#define ACC_MAX_COMP_ANGLE	0.001		// 0.5 Grad / 1000 / (57 = 180 / PI)
	#define MAG_MAX_COMP_ANGLE	0.0001


    double rot_angle_acc, rot_angle_mag;


    rot_angle_acc = acos(Quat_Acc.q0);

    if (rot_angle_acc > ACC_MAX_COMP_ANGLE)
        rot_angle_acc = ACC_MAX_COMP_ANGLE;

    Quat_Acc.q0 = cos(rot_angle_acc);


    rot_angle_mag = acos(Quat_Mag.q0);

    if (rot_angle_mag > MAG_MAX_COMP_ANGLE)
        rot_angle_mag = MAG_MAX_COMP_ANGLE;

    Quat_Mag.q0 = cos(rot_angle_mag);


    // Toggle between Acc und Mag compensation: To avoid cross effects
    if (my_toogle) {
        compensation_Quat = Q_normalise_fix_q0_zero_q3(Quat_Acc);
		#ifdef LSM_MAG_SENSOR
        my_toogle = 0;
		#endif
    } else {
        compensation_Quat = Q_normalise_fix_q0_zero12(Quat_Mag);
        my_toogle = 1;
    }

    Q_normalise(compensation_Quat);

    return compensation_Quat;
}

void Kalman_Set_RP(double roll, double pitch) {
    sensorWerte.winkel_x_kalman = roll;
    sensorWerte.winkel_y_kalman = pitch;

    roll_k.x_k[0] = roll;
    pitch_k.x_k[0] = pitch;

    xprintf("Set KF: Roll = %3.3f; Pitch = %3.3f;", roll, pitch);
}


void setRPY(double roll, double pitch, double yaw) {

		// Set RPY angles
		sensorWerte.roll_angle = roll;
		sensorWerte.pitch_angle = pitch;
		sensorWerte.yaw_angle = yaw;

		sensorWerte.roll_angle_old = roll;
		sensorWerte.pitch_angle_old = pitch;

		Kalman_Set_RP(roll, pitch);

		Quat = calcQuat(roll, pitch, yaw);
}

