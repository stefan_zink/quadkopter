
#include "AQopterI8.h"

// Aktuelle Steuerdaten des Quadcopters wie EngineOn, HeightController, etc.
steuerDaten     steuerWerte = { 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
controllerDaten controllerWerte; 

sensorDaten     sensorWerte; 
sensorDaten_raw sensorWerte_raw;

Kalman          roll_k;
Kalman          pitch_k;
