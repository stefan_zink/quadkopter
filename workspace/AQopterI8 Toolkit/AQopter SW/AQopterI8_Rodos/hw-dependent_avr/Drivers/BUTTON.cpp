/*
 * BUTTON.c
 *
 *  Created on: 03.07.2014
 *      Author: Gageik
 */

#include "Orientation.h"

extern steuerDaten steuerWerte;

extern "C"{

#include "atmel.h"			//Mandantory

#include "basics.h"
#include "QUAD_TWI_6DOF.h"			// All Header Files

// Button Interrupt
__attribute__((__interrupt__))
static void Joy_int_handler(){

			// Hier noch ein Timing hinzufügen: 		TODO SM
			//if (tc_ticks > last_button_time + 500){
			  			  steuerWerte.button_clicked = steuerWerte.button_clicked + 1;
			  			  //last_button_time = tc_ticks;
			  //		  }

			gpio_clear_pin_interrupt_flag(GPIO_JOYSTICK_PUSH);
}


__attribute__((__interrupt__))
static void dip204_example_PB_int_handler(void)				// For Debug, Obsolete to Fly
{

  /* display all available chars */
  if (gpio_get_pin_interrupt_flag(GPIO_PUSH_BUTTON_0))
  {
	#ifdef ENGINE_WITH_SWITCH_DEBUG

		  if (steuerWerte.engine_on){
			  steuerWerte.engine_on = 0;
		  }
		  else{
			  steuerWerte.engine_on = 1;
		  }
	#endif

	  gpio_clear_pin_interrupt_flag(GPIO_PUSH_BUTTON_0);

  }

  if (gpio_get_pin_interrupt_flag(GPIO_PUSH_BUTTON_1))
  {

	setRPY(10, 0, 0);

	  gpio_clear_pin_interrupt_flag(GPIO_PUSH_BUTTON_1);
  }

  if (gpio_get_pin_interrupt_flag(GPIO_PUSH_BUTTON_2))
  {

	setRPY(-10, 0, 0);
	gpio_clear_pin_interrupt_flag(GPIO_PUSH_BUTTON_2);
  }
}



void init_button_irq(){
	  /* Disable all interrupts */
	  Disable_global_interrupt();
	  gpio_enable_pin_interrupt(GPIO_JOYSTICK_PUSH , GPIO_FALLING_EDGE);
	  gpio_enable_pin_interrupt(GPIO_PUSH_BUTTON_0 , GPIO_RISING_EDGE);
	  gpio_enable_pin_interrupt(GPIO_PUSH_BUTTON_1 , GPIO_RISING_EDGE);
	  gpio_enable_pin_interrupt(GPIO_PUSH_BUTTON_2 , GPIO_RISING_EDGE);

	  INTC_register_interrupt( &Joy_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_JOYSTICK_PUSH/8), AVR32_INTC_INT1);
	  INTC_register_interrupt( &dip204_example_PB_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_PUSH_BUTTON_1/8), AVR32_INTC_INT1);
	  INTC_register_interrupt( &dip204_example_PB_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_PUSH_BUTTON_2/8), AVR32_INTC_INT1);
	  INTC_register_interrupt( &dip204_example_PB_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_PUSH_BUTTON_0/8), AVR32_INTC_INT1);

	  /* Enable all interrupts */
	  Enable_global_interrupt();
}

}


