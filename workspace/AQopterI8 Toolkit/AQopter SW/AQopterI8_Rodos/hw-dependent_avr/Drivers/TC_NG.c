/*
 * myTC.c
 *
 *  Created on: 04.08.2011
 *      Author: gageik
 */
// This Quadcopter Software Framework was developed at
// University Wuerzburg
// Chair Computer Science 8
// Aerospace Information Technology
// Copyright (c) 2011 Nils Gageik

// This Software uses the AVR Framework:
// Copyright (c) 2009 Atmel Corporation. All rights reserved.
#include "atmel.h"			//Mandantory

// ******************************************
// *		TC driver						*
// ******************************************
#include "QUAD_TWI_6DOF.h"

extern long long nanoTime;

void wait_i(unsigned int j){
	int i,k;
	for (i=0;i<j;i++)
		k++;
}

int wait_ms(unsigned int wait){

	wait_i(60000 * wait);
	return 0;

	volatile int i;
	long long t = nanoTime;
	long long end_time = wait * 1000000 + nanoTime;

	while(t < end_time){
		t = nanoTime;
		i++;
	}

	return i;
}
