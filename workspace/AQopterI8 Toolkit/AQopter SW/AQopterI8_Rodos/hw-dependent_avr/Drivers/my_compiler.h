/*
 * my_compiler.h
 *
 *  Created on: 03.07.2014
 *      Author: Gageik
 */

// W�hrend der Portierung auf Rodos wurde die compiler.h getauscht,
// der neuen Version fehlen aber ein paar Dinge, die hier aus der
// urspr�nglichen erg�nzt sind

#ifndef MY_COMPILER_H_
#define MY_COMPILER_H_

/*! \name Usual Constants
 */
//! @{
#define DISABLE   0
#define ENABLE    1
#define DISABLED  0
#define ENABLED   1
#define OFF       0
#define ON        1
#define FALSE     0
#define TRUE      1
#ifndef __cplusplus
#if !defined(__bool_true_false_are_defined)
#define false     FALSE
#define true      TRUE
#endif
#endif
#define KO        0
#define OK        1
#define PASS      0
#define FAIL      1
#define LOW       0
#define HIGH      1
#define CLR       0
#define SET       1
//! @}


#endif /* MY_COMPILER_H_ */
