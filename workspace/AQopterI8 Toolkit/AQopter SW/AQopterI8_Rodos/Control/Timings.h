/*
 * Timings.h
 *
 *  Created on: 07.07.2014
 *      Author: Gageik
 */

#ifndef TIMINGS_H_
#define TIMINGS_H_

#define SIGNAL_PROCESSING_START_TIME		2
#define FILTER_START_TIME					2
#define	ATTITUDE_CONTROL_START_TIME			2
#define MOTOR_CONTROL_START_TIME			2

#endif /* TIMINGS_H_ */
