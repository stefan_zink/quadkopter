/*
 * basics.h
 *
 *  Created on: 12.04.2011
 *      Author: gageik
 */

#pragma once
#include <stdint.h>

#define FLUG									// USE 3DOF ?!:	Switch between 3DOF == FLUG and 2DOF

// Kompensations Modi zum Experimentieren
#define			COMP_MODE_VERY_FAST
//#define		COMP_MODE_FAST
//#define		COMP_MODE_NORMAL

// Timing and Configuration Parameters
#define RENEW_DISPLAY_TIME			201
#define SAMPLE_TIME				10			// US: 5ms

#define BIAS_SAMPLES				2000		// 500 are good

#define	BATTERY_VOLTAGE				12.0f
#define SCHUB_GAS				133.0f		// Gut f�r 12V
#define NULL_GAS				3.0f		// Minimalgas jedes Motors (damit nicht ausgeht)
#define MAX_GAS					255.0f		// maximales Gas pro Motor

//#define	MINIMAL_GAS				40.0f		// 40: Startgas bei Einschalten (zzgl. Kn�ppel & Reglergas)
#define	MINIMAL_GAS				90.0f		// 40: Startgas bei Einschalten (zzgl. Kn�ppel & Reglergas)

// Programming Parameters
#define DISPLAY_MODI 				8			// Number of Display States
#define DIP_START_MODUS				2

#define DMA_PDCA								// Use PDCA DMA to Send/Receive Data
#define DEBUG_BUFFER_SIZE			200

// Debugging Modi
#define DEBUG_MSG_TWI 							// 	USART Debug Messages for TWI: Outcomment to set off
//#define DEBUG_MSG								// 	Activates Global Debugging Messages: Letters
//#define DEBUG_MSG_IMU							//  USART Debug Messages for IMU
#define DEBUG_INFORMATIONS
//#define DEBUG_THREAD_LETTERS					// Buchstaben der Activities anzeigen
//#define DEBUG_PRINT_TIME
//#define DEBUG_TIMING_1						// tc_ticks Stand in SP anzeigen
//#define DEBUG_TIMING_DIFFERENCE				// Zeitintervall in SP anzeigen


#define IMU_AUTO_DETECTION
//#define LSM_MAG_SENSOR
#define MINI_IMU_V2		// Ob Version 2 oder Version 1 benutzt wird f�r L3G & LSM

#define NEW_ORIENTATION							// Use New -> Quaternion Based Rotation (Relativ), Old -> KF Based (Absolute)

//#define ALEX_PD
//#define NONFLYING							// Motoren werden zwar angesteurt, jedoch immer auf 0 gesetzt

#define CALIBRATION_DEBUG
#define ACC_DYNAMIC_BIAS			// MAG_MINMAX_CALIBRATION = In alle Richtungen drehen und das Minimum + Maximum nehmen -> nicht gut

#define ENGINE_WITH_SWITCH_DEBUG			// 	Set Motor On by PB0, remove when Fly
//#define DISPLAY_ON							// 	Display OFF: Outcomment this line

//#define TEST_GAS_ABZUG		40
#define TEST_GAS_ABZUG			0


#define GYRO_AUSREISSER_FILTER
#define GYRO_AUSREISSER_SCHRANKE	1000			// durch ca. 30 (entspricht Skalierungsfaktor) teilen -> [�/s]

#define DIFFERENTIAL_FILTER_SIZE	10

#define CPU_SPEED			60000000			//Macht Probleme mit I2C
//#define CPU_SPEED					12000000

#define GRAVITATIONS_KONSTANTE		9.81f

#define KALMAN_FILTER							// Use Kalman Filter (this might be obsolete)

//define LED PINS
#define	INIT_READY_LED				AVR32_PIN_PB19
#define	SENSOR_CALIBRATE_LED			AVR32_PIN_PB20
#define	REMOTE_CALIBRATE_LED			AVR32_PIN_PB21
#define	RUNTIME_LED				AVR32_PIN_PB22

#define I2C_RESET_CLEAR_PIN				40

// Automation States
#define AUTOMATION_SWITCH_OFF				0
#define AUTOMATION_SWITCH_MID				1
#define AUTOMATION_SWITCH_ON				2

#ifdef FLUG
#define PROJEKT_NAME 		"	3 DOF INIT\n"
#else
#define PROJEKT_NAME 		"	2 DOF INIT\n"
#endif

// Ring buffer f�r D-Anteil Buffersize (OF)
#define OF_D_BUFFERSIZE 5

