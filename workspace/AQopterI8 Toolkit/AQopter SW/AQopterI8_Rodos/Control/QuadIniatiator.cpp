/*
 * QuadIniatiator.cpp
 *
 *  Created on: 07.07.2014
 *      Author: Gageik
 *
 *
 * AQopterI8 Software Framekwork
 *
 * This Quadcopter Software Framework was developed at
 * University Wuerzburg
 * Chair Computer Science 8
 * Aerospace Information Technology
 * Copyright (c) 2011-2014 Nils Gageik
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Translation/Portation to C++ on RODOS: SM/NG, 2014
 * */

#include "rodos.h"
#include "AQopterI8.h"
#include "QuadInitiator.h"

extern "C" {
    #include "QUAD_TWI_6DOF.h"			// All Header Files
    #include "atmel.h"			//Mandantory

    extern void Init_IMU_Sensors(); // IMU/IMU_Selector.c
}

namespace QuadInitiator {

QuadInit quadInit;

void QuadInit::init() {

    //volatile avr32_pm_t* pm = &AVR32_PM;

    /************************************************************** Hardware  *****/
    xprintf("** Quad Initiator ****************************************************\n");

    xprintf("	Start Set Up:\n");

    gpio_set_gpio_pin(INIT_READY_LED); 				// First LED means Main is starting
    gpio_clr_gpio_pin(I2C_RESET_CLEAR_PIN); 		// Set GND Pin back to GND to start Sensor

    // Disable interrupts
    //Disable_global_interrupt();

    // init the interrupts
    //INTC_init_interrupts();
    //xprintf("	INTC Set:\n");

	#ifdef DISPLAY_ON
    myDIP_init(); // Init DIP
    xprintf("	DIP Set:\n");

	#endif

    init_button_irq();
    xprintf("	Button Set:\n");

    //myTC_init(); // Init TC
    //myUSART_init(); // Init USART
    myTWI_init(); // Init TWI
    xprintf("	TWI Set:\n");

    //Enable_global_interrupt();

    /*
    int64_t continueTime = NOW() + 500*MILLISECONDS;
    BUSY_WAITING_UNTIL(continueTime);
    */

    // Init TWI Slaves
    Init_IMU_Sensors();
    xprintf("	IMU Set:\n");

    gpio_clr_gpio_pin(INIT_READY_LED);

    // Write Projekt Name for Debuggin Information to USART and on Display
    xprintf(PROJEKT_NAME);

	#ifdef DISPLAY_ON
	// ---- StartUp --------------------------------
	// *******	Display		**********************
	dip204_set_cursor_position(1,1);
	dip204_write_string(PROJEKT_NAME);
	dip204_hide_cursor();
	// ---------------------------------------------	 *
	#endif

	int64_t continueTime = NOW() + 500*MILLISECONDS;
    xprintf("	waiting until %3.6f for Hw-Init\n", (double)continueTime/(double)SECONDS);
    BUSY_WAITING_UNTIL(continueTime) ;

    gpio_set_gpio_pin(INIT_READY_LED);

    /************************************************************** Sofware *****/
    xprintf("	HW Done ... now SW\n");

    steuerWerte.engine_on = 0;
    steuerWerte.calibrate_on = 0;

    Init_MeanFilters();
    Init_Ausreisser_Filter();

    //last_Sample_time = tc_ticks;
    xprintf("	SW initialized\n");
    xprintf("*******************************************************************\n");

}

}
