/*
 * QUAD_TWI_6DOF.h
 *
 *  Created on: 16.03.2012
 *      Author: gageik
 */

#ifndef _QUAD_TWI_6DOF_H_
#define _QUAD_TWI_6DOF_H_ 

#include "Quad_Data.h"
#include "AQopterI8.h"

// HW Includes (Framework)
//#include <avr32/io.h>
#include "board.h"
#include "compiler.h"
#include "dip204.h"
#include "intc.h"
#include "tc.h"
#include "gpio.h"
#include "pm.h"
#include "delay.h"
#include "usart.h"
#include "adc.h"
#include "spi.h"
#include "pwm.h"
#include "flashc.h"
#include "power_clocks_lib.h"
#include "cycle_counter.h"
#include "pdca.h"

// Misc
#include "atmel.h"
#include "Beeper+LED.h"

// IMU
#include "ITG3200.h"
#include "ADXL345.h"
#include "LSM303DLM.h"
#include "L3G4200D.h"
#include "MPU6000.h"
#include "IMU_Selector.h"

// Drivers for Interfaces and HW
#include "DIP_NG.h"
#include "TC_NG.h"
#include "TWI_NG.h"
#include "BUTTON.h"
#include "my_compiler.h"

#endif
