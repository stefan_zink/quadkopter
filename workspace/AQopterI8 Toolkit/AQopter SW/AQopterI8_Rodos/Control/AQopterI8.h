
#pragma once

#include "stdint.h"
#include "math.h"
#include "stdlib.h"

#include "basics.h"
#include "myMath.h"
#include "Quad_Data.h"
#include "Orientation.h"
#include "MeanFilter.h"
#include "topics.h"
#include "Timings.h"

/*

Threads / Objects / Functions	(Mandantory / Required):

QuadInit quadInit;
SignalProcesor signalProcesor;
AttitudeControl attitudeControl;
MotorController motorController;


Optional (Either CQF or KF):
KalmanFilterKlasse KalmanFilterObjekt;
CQF cqf;

DebugSupport debugSupport

*/
