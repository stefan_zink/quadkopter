/*
 * AQopterI8 Software Framekwork
 *
 * This Quadcopter Software Framework was developed at
 * University Wuerzburg
 * Chair Computer Science 8
 * Aerospace Information Technology
 * Copyright (c) 2011-2014 Nils Gageik
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * Translation/Portation to C++ on RODOS: SM/NG, 2014
 * */


#include "rodos.h"
#include "activity.h"

#include "AQopterI8.h"

extern unsigned int amount_twi_resets;
extern unsigned int tc_ticks;

#define		DEBUG_SUPPORT_JITTER		0
#define		DEBUG_SUPPORT_DISPLAY		1
#define		DEBUG_SUPPORT_SENSOREN		2
#define		DEBUG_SUPPORT_TWI_RESETS	3

namespace DebugSupporter {

class DebugSupport : public Activity {
public:
    DebugSupport() : Activity("DebugSupport", 100, 2*SECONDS, 10*SAMPLE_TIME*MILLISECONDS) { id = 0;}
    DebugSupport(int my_id) : Activity("DebugSupport", 100, 2*SECONDS, 10*SAMPLE_TIME*MILLISECONDS) { id = my_id;}
    void step(int64_t timeNow);
    int id;
};

//DebugSupport debugSupport(DEBUG_SUPPORT_SENSOREN);
//DebugSupport debugSupport(DEBUG_SUPPORT_TWI_RESETS);
/********************************************************/

uint32_t jitter_counter = 0;
int64_t  lastCall = 0;
int counter = 0;

void DebugSupport::step(int64_t timeNow) {

	switch (id){

	case DEBUG_SUPPORT_JITTER:
			if((timeNow - lastCall) > (SAMPLE_TIME+2)*MILLISECONDS)  jitter_counter++;
			lastCall = timeNow;

			if (jitter_counter >= 10) {
				xprintf("ERR! 10th Lost Sample");
				jitter_counter = 0;
			}
	break;

	case DEBUG_SUPPORT_DISPLAY:

    //setDisplay();

    break;

	case DEBUG_SUPPORT_SENSOREN:

	xprintf("\n");
	xprintf("	Roll Acc: %3.6f\n", sensorWerte.roll_acc);
	xprintf("	Pitch Acc: %3.6f\n", sensorWerte.pitch_acc);
	xprintf("	Roll: %3.6f\n", sensorWerte.roll_angle);
	xprintf("	Pitch: %3.6f\n", sensorWerte.pitch_angle);
	xprintf("	Yaw: %3.6f\n", sensorWerte.yaw_angle);

	break;

	case DEBUG_SUPPORT_TWI_RESETS:

	//counter = (int)NOW();//MILLISECONDS;

	//counter++;

	xprintf("	TWI Resets: %d at %d and %d\n", amount_twi_resets, counter, tc_ticks);

	break;
	}
}

} // namespace


