

#include "rodos.h"
#include "activity.h"

#include "AQopterI8.h"


/******* Controller  ************************************/

namespace TimePrinting {

class TimePrinter : public Activity {
public:
    TimePrinter() : Activity("TimePrinter", 900, 1*SECONDS, SAMPLE_TIME*MILLISECONDS) { } 
    void step(int64_t timeNow) {xprintf("\n %6.3f :", (double)timeNow/(double)SECONDS); }
};

#ifdef DEBUG_PRINT_TIME
TimePrinter timePrinter;
#endif

} // namespace


