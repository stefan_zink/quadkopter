/*
 * QuadInitiator.h
 *
 *  Created on: 07.07.2014
 *      Author: Gageik
 */

#ifndef QUADINITIATOR_H_
#define QUADINITIATOR_H_

#include "rodos.h"

namespace QuadInitiator {

class QuadInit : public Initiator {
    void init();
};

}


#endif /* QUADINITIATOR_H_ */
