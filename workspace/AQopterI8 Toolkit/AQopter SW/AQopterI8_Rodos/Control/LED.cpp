/*
 * Timer.cpp
 *
 *  Created on: 07.07.2014
 *      Author: Gageik
 */

#include "rodos.h"

extern "C"{
#include "Beeper+LED.h"
}

extern unsigned int tc_ticks;

//static Application module01("TestTimebeats02");

class LED_Class: public Thread{
public:
	void run(){
    PRINTF("	LED Init\n");

    TIME_LOOP(1*SECONDS, 300*MILLISECONDS){
    	toggle_led();
    }

  }

};

static LED_Class led_toogle;
