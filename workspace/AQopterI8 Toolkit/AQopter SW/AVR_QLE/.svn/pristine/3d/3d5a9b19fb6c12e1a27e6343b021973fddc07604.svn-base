/*
 * PDCA_DMA_NG.c
 *
 *  Created on: 22.02.2013
 *      Author: Gageik
 */

#include "..\QUAD_TWI_6DOF.h"

// PDCA DMA INTERN COMM
// PDCA Drivers for INTERN Communication with onBoard PC (nano ITX)
// These functions are dedicated for USART Communication with onBoard Computer (intern)

// This is the lower layer to realize PDCA DMA Communication

#ifdef INTERN_COMM

pdca_Daten pdca_Intern_Werte;

// ************					INIT PDCA DMA		************************************************************
void intern_pdca_dma_usart_comm_init(){

	pdca_Intern_Werte.usart_rx_current_index = 0;
	pdca_Intern_Werte.usart_tx_current_index = 0;
	pdca_Intern_Werte.usart_tx_load_state = PDCA_STATE_BUFFER_1;
	pdca_Intern_Werte.usart_rx_load_state = PDCA_STATE_BUFFER_1;

	// TODO: to be tested
	//AVR32_HMATRIX.mcfg[AVR32_HMATRIX_MASTER_CPU_INSN] = 0x1;

	// PDCA channel options for USART TX (Transfer USART Debug)
	static const pdca_channel_options_t PDCA_OPTIONS_TX_intern =
	{
		.addr = (void *)pdca_Intern_Werte.usart_tx_buffer_1,        // memory address
		.pid = USART_COMM_INTERN_PDCA_PID_TX,           			// select peripheral - data are transmit on USART TX line.
		.size = 0,              // transfer counter
		.r_addr = (void *)pdca_Intern_Werte.usart_tx_buffer_2,      // next memory address
		.r_size = 0,            // next transfer counter
		.transfer_size = PDCA_TRANSFER_SIZE_BYTE  					// select size of the transfer
	 };

	  // Init PDCA channel with the pdca_options.
	  pdca_init_channel(PDCA_CHANNEL_USART_TX_INTERN, &PDCA_OPTIONS_TX_intern); // init PDCA channel with options.


	  static const pdca_channel_options_t PDCA_OPTIONS_RX_intern =
	  {
	    .addr = (void *)pdca_Intern_Werte.usart_rx_buffer_1,     // memory address
	    .pid = USART_COMM_INTERN_PDCA_PID_RX,     				 // select peripheral - data are transmit on USART TX line.
	    .size = PDCA_RX_BUFFER_SIZE,            				 // transfer counter
	    .r_addr = (void *)pdca_Intern_Werte.usart_rx_buffer_2,   // next memory address
	    .r_size = PDCA_RX_BUFFER_SIZE,                 			 // next transfer counter
	    .transfer_size = PDCA_TRANSFER_SIZE_BYTE  				 // select size of the transfer
	  };

	  // Init PDCA channel with the pdca_options.
	  pdca_init_channel(PDCA_CHANNEL_USART_RX_INTERN, &PDCA_OPTIONS_RX_intern); // init PDCA channel with options.

	  // Enable now the transfer.
	  pdca_enable(PDCA_CHANNEL_USART_TX_INTERN);
	  pdca_enable(PDCA_CHANNEL_USART_RX_INTERN);
}



// ************					SENT PDCA DMA		************************************************************
// Sends String-Data as Char via PDCA DMA
// The data is to be stored correctly in one of two buffers
// Parameters are pointer to char-Array (String) and length/size in Byte
void intern_pdca_dma_usart_comm_sent(char* data, unsigned int size){

	unsigned int lauf_index = minimum_ui(size, (unsigned int)PDCA_TX_BUFFER_SIZE - pdca_Intern_Werte.usart_tx_current_index);
	unsigned int i;

	switch(pdca_Intern_Werte.usart_tx_load_state){

	case PDCA_STATE_BUFFER_1:					// Speichere in Buffer
		for (i = 0; i < lauf_index; i++){
			pdca_Intern_Werte.usart_tx_buffer_1[pdca_Intern_Werte.usart_tx_current_index] = data[i];
			pdca_Intern_Werte.usart_tx_current_index++;
		}
		break;

	case PDCA_STATE_BUFFER_2:
		for (i = 0; i < lauf_index; i++){
			pdca_Intern_Werte.usart_tx_buffer_2[pdca_Intern_Werte.usart_tx_current_index] = data[i];
			pdca_Intern_Werte.usart_tx_current_index++;
		}
		break;

	case PDCA_STATE_BUFFER_3:
		for (i = 0; i < lauf_index; i++){
			pdca_Intern_Werte.usart_tx_buffer_3[pdca_Intern_Werte.usart_tx_current_index] = data[i];
			pdca_Intern_Werte.usart_tx_current_index++;
		}
		break;
	}
}


// Processes the data from the correct buffer
void intern_pdca_dma_usart_comm_process(){

	switch(pdca_Intern_Werte.usart_tx_load_state){

		case PDCA_STATE_BUFFER_1:		// Buffer 3 Loaded, Buffer 1 Reloaded, Buffer 2 to be written next

			if (!pdca_get_reload_size(PDCA_CHANNEL_USART_TX_INTERN) && pdca_Intern_Werte.usart_tx_current_index){
				pdca_reload_channel(PDCA_CHANNEL_USART_TX_INTERN, (void *)pdca_Intern_Werte.usart_tx_buffer_1, pdca_Intern_Werte.usart_tx_current_index);
				pdca_Intern_Werte.usart_tx_current_index = 0;
				pdca_Intern_Werte.usart_tx_load_state = PDCA_STATE_BUFFER_2;
			}
			break;

		case PDCA_STATE_BUFFER_2:	// Buffer 1 Loaded, Buffer 2 Reloaded, Buffer 3 to be written
			if (!pdca_get_reload_size(PDCA_CHANNEL_USART_TX_INTERN) && pdca_Intern_Werte.usart_tx_current_index){
				pdca_reload_channel(PDCA_CHANNEL_USART_TX_INTERN, (void *)pdca_Intern_Werte.usart_tx_buffer_2, pdca_Intern_Werte.usart_tx_current_index);
				pdca_Intern_Werte.usart_tx_current_index = 0;
				pdca_Intern_Werte.usart_tx_load_state = PDCA_STATE_BUFFER_3;
			}
			break;

		case PDCA_STATE_BUFFER_3:
			if (!pdca_get_reload_size(PDCA_CHANNEL_USART_TX_INTERN) && pdca_Intern_Werte.usart_tx_current_index){
				pdca_reload_channel(PDCA_CHANNEL_USART_TX_INTERN, (void *)pdca_Intern_Werte.usart_tx_buffer_3, pdca_Intern_Werte.usart_tx_current_index);
				pdca_Intern_Werte.usart_tx_current_index = 0;
				pdca_Intern_Werte.usart_tx_load_state = PDCA_STATE_BUFFER_1;
			}
		break;
		}
}


// ************					RECEIVE PDCA DMA		************************************************************

// Receive Data from pdca and write into dataStream: To be executed two times to withdraw all data (optional)
// This can be necessary as data is saved in two different buffers (with not connected arrays): But not required (just read next time)
dataStream intern_pdca_usart_comm_rec_data(void){

	dataStream mydata;		// New data is written with size to dataStream (return value)

	// Amount of Bytes in Buffer
	unsigned int size_load = PDCA_RX_BUFFER_SIZE - pdca_get_load_size(PDCA_CHANNEL_USART_RX_INTERN);


	switch(pdca_Intern_Werte.usart_rx_load_state){

	case PDCA_STATE_BUFFER_1:
		if (!pdca_get_reload_size(PDCA_CHANNEL_USART_RX_INTERN)){
			mydata.data = &pdca_Intern_Werte.usart_rx_buffer_1[pdca_Intern_Werte.usart_rx_current_index];
			mydata.size = PDCA_RX_BUFFER_SIZE - pdca_Intern_Werte.usart_rx_current_index;
			pdca_Intern_Werte.usart_rx_current_index = 0;
		}
		else if (size_load > pdca_Intern_Werte.usart_rx_current_index){
			mydata.data = &pdca_Intern_Werte.usart_rx_buffer_1[pdca_Intern_Werte.usart_rx_current_index];
			mydata.size = size_load - pdca_Intern_Werte.usart_rx_current_index;
			pdca_Intern_Werte.usart_rx_current_index = size_load;
		}
		else {
			mydata.size = 0;
		}

		break;

	case PDCA_STATE_BUFFER_2:
		if (!pdca_get_reload_size(PDCA_CHANNEL_USART_RX_INTERN)){
			mydata.data = &pdca_Intern_Werte.usart_rx_buffer_2[pdca_Intern_Werte.usart_rx_current_index];
			mydata.size = PDCA_RX_BUFFER_SIZE - pdca_Intern_Werte.usart_rx_current_index;
			pdca_Intern_Werte.usart_rx_current_index = 0;
		}
		else if (size_load > pdca_Intern_Werte.usart_rx_current_index){
			mydata.data = &pdca_Intern_Werte.usart_rx_buffer_2[pdca_Intern_Werte.usart_rx_current_index];
			mydata.size = size_load - pdca_Intern_Werte.usart_rx_current_index;
			pdca_Intern_Werte.usart_rx_current_index = size_load;
		}
		else {
			mydata.size = 0;
		}

		break;
	}

	return mydata;
}


// Sets the current/last buffer free: To be executed by user after reading and processing is finished
// So the data does not need to be copied, user can process data and afterwards same space will be overwritten
void intern_pdca_usart_comm_rec_set_free(void){

	if(!pdca_get_reload_size(PDCA_CHANNEL_USART_RX_INTERN)){

		switch(pdca_Intern_Werte.usart_rx_load_state){


		case PDCA_STATE_BUFFER_1:				// Current write to Buffer 2, next write to 1 (Reload)
			pdca_reload_channel(PDCA_CHANNEL_USART_RX_INTERN, (void*)pdca_Intern_Werte.usart_rx_buffer_1, (unsigned int)PDCA_RX_BUFFER_SIZE);
			pdca_Intern_Werte.usart_rx_load_state = PDCA_STATE_BUFFER_2;

		break;

		case PDCA_STATE_BUFFER_2:
			pdca_reload_channel(PDCA_CHANNEL_USART_RX_INTERN, (void*)pdca_Intern_Werte.usart_rx_buffer_2, (unsigned int)PDCA_RX_BUFFER_SIZE);
			pdca_Intern_Werte.usart_rx_load_state = PDCA_STATE_BUFFER_1;
		break;
		}
	}
}




// ************					API PDCA DMA		************************************************************
// Send a Stream
void intern_pdca_dma_sent_usart_comm_stream(dataStream* stream){
	intern_pdca_dma_usart_comm_sent(stream->data, stream->size);
}

// Sends one character (1 Byte: char)
void intern_pdca_usart_comm_put_char(char data){
	char datum = data;			// TODO: simplify

	intern_pdca_dma_usart_comm_sent(&datum, 1);
}

// Write a line via pdca and add a new line at the end
void intern_pdca_write_line(char *string)
{
  int i = 0;

  while (string[i] != '\0')
	  i++;

	intern_pdca_dma_usart_comm_sent(string, i);
	intern_pdca_write_newline();
}

// Write a line via pdca
void intern_pdca_write_line2(char *string)
{
  int i = 0;

  while (string[i] != '\0')
	  i++;

  intern_pdca_dma_usart_comm_sent(string, i);
}

// Write a line via pdca with iterative put_char derived from AVR Framework
void intern_pdca_write_line_put(char *string)
{
  while (*string != '\0')
	  intern_pdca_usart_comm_put_char(*string++);
}

// Write a new line ASCI symbol
void intern_pdca_write_newline(void){
	char newline = '\n';

	intern_pdca_usart_comm_put_char(newline);
}


#endif
