AQopter SW:
Software f�r den Quadrokopter (QLE Aufbauten)
- AQopterI8_Rodos: Funktionsf�hige Quadrokopter Software f�r AVR mit Rodos (Activities, wie im Vortrag)
- AVR_QLE: Funktionsf�hige Quadrokopter Software f�r AVR mit Big Loop als Musterl�sung
- LR-Labor_Frame_SS2014: Beispiel f�r ein auszuf�llendes Framework. Dieses wurde im SS 2014 den Studenten gegeben.

AVR Software:
Software zum Einrichten & Programmieren des AVR EVK1100 Development Boards

Docu:
Dokumentationen bestehend aus �bungsbl�ttern (LR-Labor), dem Einf�hrungs-Vortrag und einer AVR Readme 
