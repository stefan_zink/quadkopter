/*
 * quaternion.h
 *
 *  Created on: 24.05.2014
 *      Author: Norbert
 */

#ifndef QUATERNION_H_
#define QUATERNION_H_
#include "structs.h"
void Disp_Quat(dQuat* dq);
dQuat RPY_to_Quaternion(double dx,double dy,double dz);
void normiereQuaternion(dQuat* dq);
void SendQuatUsart(dQuat dq);
dQuat  quaternionenmultiplikation(dQuat r,dQuat dQ);
#endif /* QUATERNION_H_ */
