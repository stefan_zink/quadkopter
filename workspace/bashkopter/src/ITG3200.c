/*
 * ITG3200.c
 *
 */

// ITG Gyroscope and IMU3000

#include "basics.h"
#include "AVR_Framework.h"
#include "TWI_NG.h"
#include "TC_NG.h"
#include "ITG3200.h"
#include "structs.h"
#include <math.h>
#define SHIFT 8
#define PI 3.14159265
// #define CutOff 1.0
twi_package_t packet_itg;
extern char debug_line[DEBUG_BUFFER_SIZE];

char data_received_itg[RECEIVE_DATA_LENGTH];

const U8 setup_data_itg_PM[1] = { 0x01};
const U8 setup_data_dlpf_itg[1] =  { 0x11};
const U8 setup_data_itg_SMPLRT[1] = { 0x07};
/**
 * liest
 */
void itg3200_read(sensorDaten_raw* sensorWerte_raw){
		 // Reads from TWI
	//lel different shifting
		 // Read Twice, if reset was necessary
		 twi_read(1, &packet_itg);
		 short a=data_received_itg[0]<<SHIFT;;
		 short b=data_received_itg[1];
		 short X = a | b;
		 //short c=data_received_adxl[2]<<SHIFT;
		 short c=data_received_itg[2]<<SHIFT;
		 short d=data_received_itg[3];
		 short Y = c | d;
		  //short e=data_received_adxl[4]<<SHIFT;
		 short e=data_received_itg[4]<<SHIFT;
		 short f=data_received_itg[5];
		 short Z = e | f;//SHOOORT
		 sensorWerte_raw->gX=X;
		 sensorWerte_raw->gY=Y;
		 sensorWerte_raw->gZ=Z;

}
/**
 * zeigt sensordaten auf dip an
 */
void itg3200_toDISP_raw(sensorDaten_raw* sensorWerte_raw){
	char puffer[80];
	dip204_clear_display();
	sprintf(puffer,"itg:X %d",sensorWerte_raw->gX);
		dip_write_string(puffer,1);
	sprintf(puffer,"itg:y %d",sensorWerte_raw->gY);
		dip_write_string(puffer,2);
	sprintf(puffer,"itg:Z %d",sensorWerte_raw->gZ);
		dip_write_string(puffer,3);
	sprintf(puffer,"itg::rawww");
		dip_write_string(puffer,4);
}

/**
 * liest
 */
int itg3200_read_ex(sensorDaten_raw* sensorWerte_raw){
		 // Reads from TWI, not blocking

		 // If Read Error
		 		 if (twi_read_ex(1, &packet_itg)){
		 			 return 1;					// Error
		 		 }
		 		 itg3200_read(sensorWerte_raw);


		return 0;
}
/**
 * konditioniert
 */
void itg3200_cond(sensorDaten_raw* sensorWerte_raw){
	float scale=ITG_SCALE_FACTOR;
	sensorWerte_raw->gXcond=((float)sensorWerte_raw->gX)/scale;
	sensorWerte_raw->gYcond=((float)sensorWerte_raw->gY)/scale;
	sensorWerte_raw->gZcond=((float)sensorWerte_raw->gZ)/scale;
}

/**
 * Kalibriert
 */
void itg3200_calib(kalibrierung* KaliKaliKali,int kalibrierungen){
	USART_schreiben("Kalibrieren des ITG ....");
	USART_schreiben("....");
	dip204_clear_display();
	dip_write_string("ITG Kali",1);
	int i=0;
	//erfassung der maximal ausschlaege betraege
	//TODO dynamisierte maximal cuttoff kalibrierung
	//gemittelte werte
	double x;
	double y;
	double z;
	sensorDaten_raw temp;
	//sensorDaten_raw maxi;
	for(i=0;i<kalibrierungen;i++){
		itg3200_read(&temp);
				x+=(temp.gX);
				y+=(temp.gY);//casen is iwie strange mal mit mal ohne klammern lelvoid itg3200Kali_toDISP(kalibrierung* kalikalikali);
				z+=(temp.gZ);

		wait_ms(5);
	}
	//maximal schwankung mit skalierungsfaktor teilen

	char puff[20];
	USART_schreiben("KaliSum\n");
		sprintf(puff,"X: %lf",x);
		USART_schreiben(puff);
		sprintf(puff,"Y: %lf",y);
		USART_schreiben(puff);
		sprintf(puff,"Z: %lf",z);
		USART_schreiben(puff);
	x/=kalibrierungen;
	y/=kalibrierungen;
	z/=kalibrierungen;
	USART_schreiben("KaliSom\n");
		sprintf(puff,"X: %lf",x);
		USART_schreiben(puff);
		sprintf(puff,"Y: %lf",y);
		USART_schreiben(puff);
		sprintf(puff,"Z: %lf",z);
		USART_schreiben(puff);
	KaliKaliKali->gyro_X=(short)x;
	KaliKaliKali->gyro_Y=(short)y;
	KaliKaliKali->gyro_Z=(short)z;//-((short)(981*ITG_SCALE_FACTOR));
	itg3200_cond(&temp);
	KaliKaliKali->gyro_X_cond=temp.gXcond;
	KaliKaliKali->gyro_Y_cond=temp.gYcond;
	KaliKaliKali->gyro_Z_cond=(temp.gZcond);//;-981);
	dip_write_string("...DONE",2);
	wait_ms(1000);
}
/**
 * zeigt kalibrierung auf dip an	KONDitioniert
 */
void itg3200Kali_toDISP_cond(kalibrierung* kalikalikali)
{
	char puffer[20];
			dip204_clear_display();
			sprintf(puffer,"itg:X %lf",kalikalikali->gyro_X_cond);
				dip_write_string(puffer,1);
			sprintf(puffer,"itg:y %lf",kalikalikali->gyro_Y_cond);
				dip_write_string(puffer,2);
			sprintf(puffer,"itg:Z %lf",kalikalikali->gyro_Z_cond);
				dip_write_string(puffer,3);
			sprintf(puffer,"itg::kali::cond");
				dip_write_string(puffer,4);
}
/**
 * zeigt kalibrierung auf dip an 	RAW
 */
void itg3200Kali_toDISP_raw(kalibrierung* kalikalikali)
{
	char puffer[20];
		dip204_clear_display();
		sprintf(puffer,"itg:X %d",kalikalikali->gyro_X);
			dip_write_string(puffer,1);
		sprintf(puffer,"itg:y %d",kalikalikali->gyro_Y);
			dip_write_string(puffer,2);
		sprintf(puffer,"itg:Z %d",kalikalikali->gyro_Z);
			dip_write_string(puffer,3);
		sprintf(puffer,"itg::kali::raw");
			dip_write_string(puffer,4);
}
/**
 * macht aus den winkelgeschwindigkeiten vom itg winkel
 *
 */
void itg3200_winkel(sensorDaten* sensorWerte,winkel* winkl,int zeit){
	double l=zeit * 0.001;//samplezeit to milisekunden dingens
		winkl->x_current=((sensorWerte->gyro_x_bias)*l);
		winkl->x+=winkl->x_current;
		winkl->y_current=((sensorWerte->gyro_y_bias)*l);
		winkl->y+=winkl->y_current;
		winkl->z_current=((sensorWerte->gyro_z_bias)*l);
		winkl->z+=winkl->z_current;
}

/**
 * @DEPRECATED macht grad nix, sollte maln winkel oder so hervorbringen glaub ich
 */
void itg_kalm_winkel(Kalman_Achse* nick,Kalman_Achse* roll,sensorDaten* sensorWerte,winkel* winkl,int zeit){
	double l=zeit * 0.001;
	winkl->x_current=((nick->Xk[1])*l);
	winkl->x+=winkl->x_current;
	winkl->y_current=((roll->Xk[1])*l);
	winkl->y+=winkl->y_current;
	winkl->z_current=((sensorWerte->gyro_z_bias)*l);
	winkl->z+=winkl->z_current;

}
/**
 * winkel aufs display anzeigen
 */
void itg3200_winkel_toDISP(winkel* winkl){
	char puffer[40];
			dip204_clear_display();
			sprintf(puffer,"itg:X %lf",winkl->x);
				dip_write_string(puffer,1);
			sprintf(puffer,"itg:y %lf",winkl->y);
				dip_write_string(puffer,2);
			sprintf(puffer,"itg:Z %lf",winkl->z);
				dip_write_string(puffer,3);
			sprintf(puffer,"itg::winkel::");
				dip_write_string(puffer,4);
//	DispMessure();
}

/**
 * conditionierte winkelgeschwindigkeiten anzeigen
 */
void itg3200_toDISP_cond(sensorDaten_raw* sensorWerte_raw){
	char puffer[20];
	dip204_clear_display();
	sprintf(puffer,"itg:X %lf",sensorWerte_raw->gXcond);
		dip_write_string(puffer,1);
	sprintf(puffer,"itg:y %lf",sensorWerte_raw->gYcond);
		dip_write_string(puffer,2);
	sprintf(puffer,"itg:Z %lf",sensorWerte_raw->gZcond);
		dip_write_string(puffer,3);
	sprintf(puffer,"itg::cond");
		dip_write_string(puffer,4);

}
/**
 * kalibrierung einrechnen und so
 */
void itg3200_bias(sensorDaten_raw* sensorWerte_raw,sensorDaten* sensorWerte,kalibrierung* kalikali){
	//USART_schreiben("itg bias kalib");
	//raw
	sensorWerte->gyro_x=(double)sensorWerte_raw->gX - kalikali->gyro_X;
	sensorWerte->gyro_y=(double)sensorWerte_raw->gY - kalikali->gyro_Y;
	sensorWerte->gyro_z=(double)sensorWerte_raw->gZ - kalikali->gyro_Z;
	//conditioned
	sensorWerte->gyro_x_bias=(double)sensorWerte_raw->gXcond - kalikali->gyro_X_cond;
	sensorWerte->gyro_y_bias=(double)sensorWerte_raw->gYcond - kalikali->gyro_Y_cond;
	sensorWerte->gyro_z_bias=(double)sensorWerte_raw->gZcond - kalikali->gyro_Z_cond;// - 981;
}
/**
 * anzeigen
 */
void itg3200_toDISP_bias(sensorDaten* sensorWerte){
	//USART_schreiben("disp itg kalib");
	char puffer[50];
	dip204_clear_display();
	sprintf(puffer,"itg:X %5.1lf %5.1lf",sensorWerte->gyro_x,sensorWerte->gyro_x_bias);
		dip_write_string(puffer,1);
	sprintf(puffer,"itg:y %5.1lf %5.1lf",sensorWerte->gyro_y,sensorWerte->gyro_y_bias);
		dip_write_string(puffer,2);
	sprintf(puffer,"itg:Z %5.1lf %5.1lf",sensorWerte->gyro_z,sensorWerte->gyro_z_bias);
		dip_write_string(puffer,3);
	sprintf(puffer,"itg::Kalibrated");
		dip_write_string(puffer,4);

}
/**
 * init
 */
int itg3200_init(void){
		// Initialize ITG

		int status = 0;		// For Debug / Error Message

		// --------- 	Setup ITG3200	--------------------------------------
		// DLPF, Full Scale (Digital Low Pass Filter and Scale)
		// TWI chip address to communicate with
		packet_itg.chip = ITG_TWI_ADDRESS;
		// TWI address/commands to issue to the other chip (node)
		packet_itg.addr = ITG_INIT_DLPF_ADDR_START;
		// Length of the TWI data address segment (1-3 bytes)
		packet_itg.addr_length = EEPROM_ADDR_LGT;
		// Where to find the data to be written
		packet_itg.buffer = (void*) setup_data_dlpf_itg;
		// How many bytes do we want to write
		packet_itg.length = 1;

		// Set ITG Digital Filter and Scale
		//while (status != TWI_SUCCESS)
			status += twi_master_write_ex_edit(&AVR32_TWI, &packet_itg);

		// TWI chip address to communicate with
		packet_itg.chip = ITG_TWI_ADDRESS;
		// TWI address/commands to issue to the other chip (node)
		packet_itg.addr = ITG_INIT_PM_ADDR_START;
		// Length of the TWI data address segment (1-3 bytes)
		packet_itg.addr_length = EEPROM_ADDR_LGT;
		// Where to find the data to be written
		packet_itg.buffer = (void*) setup_data_itg_PM;
		// How many bytes do we want to write
		packet_itg.length = 1;


		// Set ITG PM, x Gyro as Reference
		//while (status != TWI_SUCCESS)
			status += twi_master_write_ex_edit(&AVR32_TWI, &packet_itg);


		// TWI chip address to communicate with
		packet_itg.chip = ITG_TWI_ADDRESS;
		// TWI address/commands to issue to the other chip (node)
		packet_itg.addr = ITG_SMPLRT_DIV;
		// Length of the TWI data address segment (1-3 bytes)
		packet_itg.addr_length = EEPROM_ADDR_LGT;
		// Where to find the data to be written
		packet_itg.buffer = (void*) setup_data_itg_SMPLRT;
		// How many bytes do we want to write
		packet_itg.length = 1;


		// Set ITG Sample Rate (here 8ms)
		//while (status != TWI_SUCCESS)
			status += twi_master_write_ex_edit(&AVR32_TWI, &packet_itg);

		#ifdef DEBUG_MSG_TWI
		if (status != 0){
			sprintf(debug_line, "\n!! TWI ITG Sensor INIT Error %d !!\n", status);
			USART_schreiben(debug_line);
		}
		#endif

		// TWI chip address to communicate with
		packet_itg.chip = ITG_TWI_ADDRESS;
		// TWI address/commands to issue to the other chip (node)
		packet_itg.addr = ITG_SENSING_ADDR_START;
		// Length of the TWI data address segment (1-3 bytes)
		packet_itg.addr_length = EEPROM_ADDR_LGT;
		// Where to find the data to be written
		packet_itg.buffer = (void*) data_received_itg;
		// How many bytes do we want to write
		packet_itg.length = RECEIVE_DATA_LENGTH;

		return status;

}
