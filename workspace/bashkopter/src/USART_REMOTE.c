/*
 * USART_REMOTE.c
 *
 *  Created on: 09.08.2011
 *      Author: gageik
 */
// This Quadcopter Software Framework was developed at
// University Wuerzburg
// Chair Computer Science 8
// Aerospace Information Technology
// Copyright (c) 2011 Nils Gageik

// This Software uses the AVR Framework:
// Copyright (c) 2009 Atmel Corporation. All rights reserved.
#include "atmel.h"			//Mandantory

// ******************************************
// *		Spektrum DX7 to USART driver	*
// ******************************************
#include "..\QUAD_TWI_6DOF.h"

extern remoteDaten remoteWerte;
extern remoteBuffer remoteBufferWerte;

extern short engine_on;						// Activates Motors

extern unsigned int tc_ticks;
extern unsigned int last_Remote_time;

extern char debug_line[DEBUG_BUFFER_SIZE];

// USART options.
static const usart_options_t USART2_OPTIONS =
{
  .baudrate     = USART2_BAUDRATE,
  .charlength   = 8,
  .paritytype   = USART_NO_PARITY,
  .stopbits     = USART_1_STOPBIT,
  .channelmode  = USART_NORMAL_CHMODE
};


short check_borders(short min, short max, short value){
// Pr�ft, ob der Wert innerhalb der definierten Grenze liegt (vorgegeben):
// Wenn ja, gibt Wert 1 zur�ck, wenn nein, gibt Wert 0

	if ((min - REMOTE_TOLERANZ < value)&&(max + REMOTE_TOLERANZ> value))
		return 1;	// Wert liegt im Intervall

	return 0;
}


void Remote(){
	// Operate on Remote

	Debug_Msg('R');

	get_remote();			// Read Remote Values from Buffer
	remote_parse();			// Parse Remote Values
	//setRemote();			// Use Remote Values, write to controllerWerte

	Debug_Msg('Q');
}


void get_remote(){
	// Decodiere neue Bytes
	// Ziehe remote_decode aus dem IRQ um die Last zu reduzieren

	short dc;

	while(remoteBufferWerte.buffer_index != remoteBufferWerte.read_index){
		dc = remote_decode(remoteBufferWerte.buffer[remoteBufferWerte.read_index]);
		remoteBufferWerte.read_index = remoteBufferWerte.read_index + 1;

		if (remoteBufferWerte.read_index == REMOTE_BUFFER_SIZE)
			remoteBufferWerte.read_index = 0;
	}
}


short remote_decode(char newByte){
	// return  -1 for error,
	//			0  for no new value
	//			1  for new value

	// Use greater buffer to avoid data hazards (read and write buffer in a ring)
	// state 0-15 write in first buffer, read from 2nd buffer
	// state 16-32 write in second buffer, read from 1st buffer

	if(remoteWerte.state == 0){								// wait for StartSignal 1. Byte
		if(newByte == REMOTE_START_BYTE_1){
			remoteWerte.state = 1;
			return 0;
		}

		return -1;
	}
	else {
			if (remoteWerte.state == 1){				// wait for StartSignal 2. Byte
					if(newByte == REMOTE_START_BYTE_2){
						remoteWerte.state = 2;
						return 0;
					}
					remoteWerte.state = 0;
					return -1;
			}

			else {										// 14 weitere Bytes sind Nutzdaten

					if (remoteWerte.state < 2 || remoteWerte.state > 15){
						return -1;					// error
						remoteWerte.state = 0;
					}

					remoteWerte.FRAME[remoteWerte.state-2] = newByte;
					remoteWerte.state++;

					if (remoteWerte.state == 16){
							remoteWerte.new_values = 1;			// New package
							remoteWerte.state = 0;
							return 1;
					}
			}

			return 0;
	}
}


void remote_parse(void){

	short roll, pitch, yaw, throttle;

	if(tc_ticks - last_Remote_time > REMOTE_TIME_OUT)
		remoteWerte.active = 0;

	if(remoteWerte.new_values == 1){

		roll 							= (short)((remoteWerte.FRAME[0] << 8) | remoteWerte.FRAME[1]);
		remoteWerte.flightmode_switch 	= (short)((remoteWerte.FRAME[2] << 8) | remoteWerte.FRAME[3]);
		pitch 							= (short)((remoteWerte.FRAME[4] << 8) | remoteWerte.FRAME[5]);
		yaw 							= (short)((remoteWerte.FRAME[6] << 8) | remoteWerte.FRAME[7]);
		throttle 						= (short)((remoteWerte.FRAME[8] << 8) | remoteWerte.FRAME[9]);
		remoteWerte.gear_switch 		= (short)((remoteWerte.FRAME[10] << 8) | remoteWerte.FRAME[11]);
		remoteWerte.aux2_switch_raw 	= (short)((remoteWerte.FRAME[12] << 8) | remoteWerte.FRAME[13]);

		// Check if received remote value is within defined borders
		if (check_borders(REMOTE_ROLL_MIN, REMOTE_ROLL_MAX, roll))
				remoteWerte.roll = roll - remoteWerte.roll_bias;
		if (check_borders(REMOTE_PITCH_MIN, REMOTE_PITCH_MAX, pitch))
				remoteWerte.pitch =  pitch - remoteWerte.pitch_bias;
		if (check_borders(REMOTE_YAW_MIN, REMOTE_YAW_MAX, yaw))
				remoteWerte.yaw  = yaw - remoteWerte.yaw_bias;
		if (check_borders(REMOTE_THROTTLE_MIN, REMOTE_THROTTLE_MAX, throttle))
				remoteWerte.throttle = throttle - remoteWerte.throttle_bias;

		#ifdef ENABLE_LOOPINGS
		remoteWerte.looping_roll = LOOPING_INACTIVE;
		remoteWerte.looping_pitch = LOOPING_INACTIVE;

		if (remoteWerte.yaw > LOOPING_STICK_THRESH)
			remoteWerte.looping_roll = LOOPING_NEGATIVE_DIR;

		if (remoteWerte.yaw < -LOOPING_STICK_THRESH)
			remoteWerte.looping_roll = LOOPING_POSITIVE_DIR;

		/*
		if (remoteWerte.throttle > LOOPING_STICK_THRESH)
			remoteWerte.looping_pitch = LOOPING_POSITIVE_DIR;

		if (remoteWerte.throttle < -LOOPING_STICK_THRESH)
			remoteWerte.looping_pitch = LOOPING_NEGATIVE_DIR;
			*/

		remoteWerte.yaw = 0;
		#endif

		remoteWerte.soll_angle_roll = (double)remoteWerte.roll * 2 * MAX_ANGLE / (REMOTE_ROLL_MAX - REMOTE_ROLL_MIN);
		remoteWerte.soll_angle_pitch = (double)remoteWerte.pitch * 2 * MAX_ANGLE / (REMOTE_PITCH_MAX - REMOTE_PITCH_MIN);
		remoteWerte.yaw = remoteWerte.yaw / 10;
		remoteWerte.soll_angle_yaw += (double)remoteWerte.yaw * REMOTE_SAMPLE_TIME * 3.0f;

		 // In Case of Remote Error: Limit Remote Input
		remoteWerte.soll_angle_roll = my_saturate(remoteWerte.soll_angle_roll, MAX_ANGLE);
		remoteWerte.soll_angle_pitch = my_saturate(remoteWerte.soll_angle_pitch, MAX_ANGLE);


		// Bei Hin-Her Bewegung der Funkkn�ppel bleiben minimale Winkel bei Nullstellung
		// Diese sollen nicht in die Steuerung eingehen: Schneide Soll_Winkel < MIN_ANGLE_SOLL ab
		if (abs(remoteWerte.soll_angle_roll) < MIN_ANGLE_SOLL)
		  remoteWerte.soll_angle_roll = 0;

		if (abs(remoteWerte.soll_angle_pitch) < MIN_ANGLE_SOLL)
		  remoteWerte.soll_angle_pitch = 0;

		if (abs(remoteWerte.soll_angle_yaw) < MIN_ANGLE_SOLL)
		  remoteWerte.soll_angle_yaw = 0;


		if (remoteWerte.throttle < 0)
			remoteWerte.throttle = 0;

		remoteWerte.gas = (double) remoteWerte.throttle / 5.0f;

		// Set Automation State
		remoteWerte.automation_state = AUTOMATION_SWITCH_OFF;
		if ((remoteWerte.flightmode_switch < REMOTE_FLT_MODE_1 + REMOTE_TOLERANZ)
				&& (remoteWerte.flightmode_switch > REMOTE_FLT_MODE_1 - REMOTE_TOLERANZ))
			remoteWerte.automation_state = AUTOMATION_SWITCH_MID;
		if ((remoteWerte.flightmode_switch < REMOTE_FLT_MODE_2 + REMOTE_TOLERANZ)
					&& (remoteWerte.flightmode_switch > REMOTE_FLT_MODE_2 - REMOTE_TOLERANZ))
			remoteWerte.automation_state = AUTOMATION_SWITCH_ON;


		if ((remoteWerte.gear_switch < REMOTE_GEAR_HIGH + REMOTE_TOLERANZ)
			&& (remoteWerte.gear_switch > REMOTE_GEAR_HIGH - REMOTE_TOLERANZ))
			remoteWerte.gear_switch = 1;
		else
			remoteWerte.gear_switch = 0;


		if ((remoteWerte.aux2_switch_raw < REMOTE_AUX2_HIGH + REMOTE_TOLERANZ)
			&& (remoteWerte.aux2_switch_raw > REMOTE_AUX2_HIGH - REMOTE_TOLERANZ))
			remoteWerte.aux2_switch = 1;
		else
			remoteWerte.aux2_switch = 0;

		remoteWerte.new_values = 0;
		remoteWerte.active = 1;
		last_Remote_time = tc_ticks;
	}
}

void remote_calibrate(){

	  int time = 0;

	  remoteWerte.new_values = 0;
	  remoteWerte.active = 0;
	  remoteWerte.calibrated = 0;

	  USART_schreiben("\nStart Remote Calibration\n");

	  time = tc_ticks;
	  while(remoteWerte.new_values != 1){
		  gpio_tgl_gpio_pin(REMOTE_CALIBRATE_LED);
		  get_remote();
		  if  (tc_ticks > time + REMOTE_INIT_TIME_OUT){
			  gpio_clr_gpio_pin(REMOTE_CALIBRATE_LED);
			  USART_schreiben("Remote Calibration Failed\n");
			  return;
		  }
	  }

	  remoteWerte.roll_bias		= ((short)((remoteWerte.FRAME[0] << 8) | remoteWerte.FRAME[1]));
	  remoteWerte.throttle_bias = ((short)((remoteWerte.FRAME[8] << 8) | remoteWerte.FRAME[9]));
	  remoteWerte.pitch_bias 	= ((short)((remoteWerte.FRAME[4] << 8) | remoteWerte.FRAME[5]));
	  remoteWerte.yaw_bias 		= ((short)((remoteWerte.FRAME[6] << 8) | remoteWerte.FRAME[7]));

	  // Read Gear & Aux Switch for Testing: Only Calibrate if OFF
	  remoteWerte.gear_switch 		= (short)((remoteWerte.FRAME[10] << 8) | remoteWerte.FRAME[11]);
	  remoteWerte.aux2_switch 		= (short)((remoteWerte.FRAME[12] << 8) | remoteWerte.FRAME[13]);

	  if ((remoteWerte.gear_switch < REMOTE_GEAR_HIGH + REMOTE_TOLERANZ)
	  			&& (remoteWerte.gear_switch > REMOTE_GEAR_HIGH - REMOTE_TOLERANZ))
	  			remoteWerte.gear_switch = 1;
	  		else
	  			remoteWerte.gear_switch = 0;

	if ((remoteWerte.aux2_switch < REMOTE_AUX2_HIGH + REMOTE_TOLERANZ)
		&& (remoteWerte.aux2_switch > REMOTE_AUX2_HIGH - REMOTE_TOLERANZ))
				remoteWerte.aux2_switch = 1;
	else
				remoteWerte.aux2_switch = 0;

	  // Power & Gear_Switch muss Anfangs aus sein, sonst startet Qopter sofort, ggf. mit H�henregelung -> Gef�hrlich
	  if ((remoteWerte.gear_switch) || (remoteWerte.aux2_switch))
		  remoteWerte.calibrated = 0;
	  else{
		  remoteWerte.active = 1;					// Aktiviere Remote
		  remoteWerte.calibrated = 1;
		  remoteWerte.new_values = 0;
	  }

	  last_Remote_time = tc_ticks;

	  gpio_set_gpio_pin(REMOTE_CALIBRATE_LED);
	  USART_schreiben("Remote Calibration Finished\n");
}


__attribute__((__interrupt__))
static void usart_int_handler_2(void)
{
  int remoteUSART_Wert;
  int read_char;

  read_char = usart_read_char(USART_2, &remoteUSART_Wert);

  if(read_char){				// Transmission error: Reset USART & Remote
	  //USART_schreibe_char(r);
	  myREMOTE_reset();
  }
  else {					// Character Correct Received
	 //USART_schreibe_char('k');
	 //remoteBufferWerte.buffer_index = (remoteBufferWerte.buffer_index + 1) % REMOTE_BUFFER_SIZE;
	  remoteBufferWerte.buffer_index = remoteBufferWerte.buffer_index + 1;

	  if (remoteBufferWerte.buffer_index == REMOTE_BUFFER_SIZE)
				  remoteBufferWerte.buffer_index = 0;

	 remoteBufferWerte.buffer[remoteBufferWerte.buffer_index] = (char)(remoteUSART_Wert);
  }
}


void myREMOTE_reset(){

	USART_2->idr = AVR32_USART_IDR_RXRDY_MASK;
	usart_reset(USART_2);

	static const gpio_map_t USART_2_GPIO_MAP =
	{
		{USART_2_RX_PIN, USART_2_RX_FUNCTION},
		{USART_2_TX_PIN, USART_2_TX_FUNCTION}
	};


	// Assign GPIO to USART 2
	gpio_enable_module(USART_2_GPIO_MAP,
						sizeof(USART_2_GPIO_MAP) / sizeof(USART_2_GPIO_MAP[0]));


	// Initialize USART 2 in RS232 mode.
	usart_init_rs232(USART_2, &USART2_OPTIONS, CPU_SPEED);

	// Register the USART interrupt handler to the interrupt controller.
	// usart_int_handler is the interrupt handler to register.
	// EXAMPLE_USART_IRQ is the IRQ of the interrupt handler to register.
	// AVR32_INTC_INT0 is the interrupt priority level to assign to the group of
	// this IRQ.
	INTC_register_interrupt(&usart_int_handler_2, USART_2_IRQ, AVR32_INTC_INT1);

	// Enable USART Rx interrupt.
	USART_2->ier = AVR32_USART_IER_RXRDY_MASK;

	remoteBufferWerte.buffer_index = 0;
	remoteBufferWerte.read_index = 0;
	remoteWerte.state = 0;
	remoteWerte.active = 0;

	USART_2->ier = AVR32_USART_IER_RXRDY_MASK;
	USART_2->idr = AVR32_USART_IDR_RXRDY;
}

void myREMOTE_init(){

	static const gpio_map_t USART_2_GPIO_MAP =
		{
		    {USART_2_RX_PIN, USART_2_RX_FUNCTION},
		    {USART_2_TX_PIN, USART_2_TX_FUNCTION}
		};


	  // Assign GPIO to USART 2
	  gpio_enable_module(USART_2_GPIO_MAP,
		                    sizeof(USART_2_GPIO_MAP) / sizeof(USART_2_GPIO_MAP[0]));


	  // Initialize USART 2 in RS232 mode.
	  usart_init_rs232(USART_2, &USART2_OPTIONS, CPU_SPEED);


	  // Register the USART interrupt handler to the interrupt controller.
	  // usart_int_handler is the interrupt handler to register.
	  // EXAMPLE_USART_IRQ is the IRQ of the interrupt handler to register.
	  // AVR32_INTC_INT0 is the interrupt priority level to assign to the group of
	  // this IRQ.
	  INTC_register_interrupt(&usart_int_handler_2, USART_2_IRQ, AVR32_INTC_INT1);

	  // Enable USART Rx interrupt.
	  USART_2->ier = AVR32_USART_IER_RXRDY_MASK;

	  last_Remote_time = tc_ticks;
	  remoteBufferWerte.buffer_index = 0;
	  remoteBufferWerte.read_index = 0;
	  remoteWerte.state = 0;

	  remoteWerte.soll_angle_yaw = 0;
}

void activate_remote(){
	remoteWerte.active = 1;			// Activate Remote
	last_Remote_time = tc_ticks;
}
