/*
 * ITG3200.h
 *
 */

// ITG Gyroscope
#ifndef ITG3200_H_
	#define ITG3200_H_

	#include "../basics/basics.h"
	#include "../basics/structs.h"
	#include "../avr-driver/AVR_Framework.h"
	#include "../avr-driver/TWI_NG.h"
	#include "../avr-driver/TC_NG.h"
	#include "../sensors/ITG3200.h"
	#include <math.h>
	#include <string.h>

	#define ITG_SCALE_FACTOR	32.8f*2/3

	#define ITG_TWI_ADDRESS      		0x68    // ITG TWI address
	#define ITG_SENSING_ADDR_START 		0x1D    // Start->Address of ITG Sensor Values
	#define ITG_INIT_DLPF_ADDR_START 	0x16    // Configuration Address for ITG
	#define ITG_INIT_PM_ADDR_START 		0x3E    // PowerManagement Setup: X Gyro as reference clock (recommended)
	#define ITG_SMPLRT_DIV				0x15	// Sample Rate Divider: 8 -> 8ms per Sample bei 1kHz



	int itg3200_init(void);
	void itg3200_read(sensorDaten_raw* sensorWerte_raw);
	int itg3200_read_ex(sensorDaten_raw* sensorWerte_raw);
	void itg3200_cond(sensorDaten_raw* sensorWerte_raw);
	void itg3200_toDISP_raw(sensorDaten_raw* sensorWerte_raw);
	void itg3200_toDISP_cond(sensorDaten_raw* sensorWerte_raw);
	void itg3200_calib(kalibrierung* KaliKaliKali,int kalibrierungen);
	void itg3200Kali_toDISP_cond(kalibrierung* kalikalikali);
	void itg3200Kali_toDISP_raw(kalibrierung* kalikalikali);
	void itg3200_bias(sensorDaten_raw* sensorWerte_raw,sensorDaten* sensorWerte,kalibrierung* kalikali);
	void itg3200_toDISP_bias(sensorDaten* sensorWerte);
	void itg3200_winkel(sensorDaten* sensorWerte,winkelstruct* winkl,int zeit);
	void itg3200_winkel_toDISP(winkelstruct* winkl);
	void itg_kalm_winkel(kalmanstruct* x,kalmanstruct* y,sensorDaten* sensorWerte,winkelstruct* winkl,int zeit);

#endif /* ITG3200_H_ */
