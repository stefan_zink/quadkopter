#include "sensormanagement.h"

void __Messung(sensorDaten_raw* sensorWerte_raw, sensorDaten* sensorWerte,
		kalibrierung* kalibrierungStruct) {
	//werte lesen
	adxl345_read(sensorWerte_raw);
	itg3200_read(sensorWerte_raw);

	//werte konditionieren
	adxl345_cond(sensorWerte_raw);
	itg3200_cond(sensorWerte_raw);

	//bias is des teil iwie wo die kalibrierung drauffaellt
	adx1345_bias(sensorWerte_raw, sensorWerte, kalibrierungStruct);
	itg3200_bias(sensorWerte_raw, sensorWerte, kalibrierungStruct);
}
