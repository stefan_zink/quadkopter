/*
 * coreconfig.h
 *
 *  Created on: 04.06.2014
 *      Author: Norbert
 */

#ifndef CORECONFIG_H_
#define CORECONFIG_H_
typedef struct{
	//PID
	double Pa;double Da;double Ia;//Attitutde
	double Py;double Dy;double Iy;//Yaw
	double P_acc;double P_gyro;double M_acc;double M_gyro;//Kalmann
	unsigned int sample_time;//samplezeit in ms
	int fpt_limit;
	char configname[15];//platz um der config nen namen zu geben ggf fuers disp
	double base_speed;//engine ground speed
	int kalibrierungsanzahl;//anzahl der kalibrierungen
} base_config;

#endif /* CORECONFIG_H_ */

void initConfig(base_config* c);
