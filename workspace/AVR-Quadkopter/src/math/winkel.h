#ifndef WINKEL_H_
#define WINKEL_H_
#include <math.h>
#include "../basics/structs.h"
#include "timer.h"

void initDrehwinkel(winkelstruct* winkel);
void berechneDrehwinkel(sensorDaten* sensorWerte, winkelstruct* winkel, unsigned int sample_time);
		//timestruct* time);
void berechneBeschleunigungswinkel(sensorDaten* sensorWerte,
		winkelstruct* winkel);
#endif
