/*
 * quaternion.c
 */

#include "quaternion.h"
#include "../avr-driver/AVR_Framework.h"
#include "myMath.h"
#include "../basics/structs.h"
#include "../basics/basics.h"
#include <math.h>
#define PI 3.14159265

/**
 * ITG , also RPY (dx, dy, dz) to QUATERNION (q0, q1, q2, q3)
 */
dQuat RPY_to_Quaternion(double dx, double dy, double dz){
	dQuat erg;

	// winkelmass in radialmass umrechnen
	double dx_rad	= dx * (PI / 180) * 0.5;
	double dy_rad	= dy * (PI / 180) * 0.5;
	double dz_rad	= dz * (PI / 180) * 0.5;

	// quaternionen aus radialmassen berechnen
	erg.q0 	= ( cos(dz_rad) * cos(dy_rad) * cos(dx_rad) )
			+ sin(dz_rad) * sin (dy_rad) * sin (dx_rad);
	erg.q1	= ( cos(dz_rad) * cos(dy_rad) * sin(dx_rad) )
			- (sin(dz_rad)	* sin (dy_rad) * cos (dx_rad));
	erg.q2	= ( cos(dz_rad) * sin(dy_rad) * cos(dx_rad) )
			+ 	(sin(dz_rad)	* cos (dy_rad) * sin (dx_rad));
	erg.q3	= ( sin(dz_rad) * cos(dy_rad) * cos(dx_rad) )
			- (cos(dz_rad)	* sin (dy_rad) * sin (dx_rad));

	return erg;
}

/**
 * methode zum darstellen eines quaterionens auf einem display
 */
void Disp_Quat(dQuat* dq){
	char puffer[50];
	dip204_clear_display();
	sprintf(puffer,"q0\t%lf",dq->q0);
		dip_write_string(puffer,1);
	sprintf(puffer,"q1\t%lf",dq->q1);
		dip_write_string(puffer,2);
	sprintf(puffer,"q2\t%lf",dq->q2);
		dip_write_string(puffer,3);
	sprintf(puffer,"q3\t%lf",dq->q3);
		dip_write_string(puffer,4);

}
/**
 * NORIMERT N QUATERNION
 */
void normiereQuaternion(dQuat* dq){
	 double betrag;
	 betrag= (dq->q0 * dq->q0) + (dq->q1 * dq->q1) + (dq->q2 * dq->q2) + (dq->q3 * dq->q3);
	 betrag=sqrt(betrag);
	 if(betrag==0);
	 else{
		 dq->q0/=betrag;
		 dq->q1/=betrag;
		 dq->q2/=betrag;
		 dq->q3/=betrag;
	 }
}
/**
 * Multipliziert ein quaternion
 */
dQuat  quaternionenmultiplikation(dQuat r,dQuat dQ){
	dQuat Quat;
	Quat.q0=(dQ.q0 * r.q0 ) - (dQ.q1 * r.q1) - (dQ.q2 * r.q2) -	(dQ.q3 * r.q3);
	Quat.q1=(dQ.q0 * r.q1 ) + (dQ.q1 * r.q0) - (dQ.q2 * r.q3) +	(dQ.q3 * r.q2);
	Quat.q2=(dQ.q0 * r.q2 ) + (dQ.q1 * r.q3) + (dQ.q2 * r.q0) -	(dQ.q3 * r.q1);
	Quat.q3=(dQ.q0 * r.q3 ) - (dQ.q1 * r.q2) + (dQ.q2 * r.q1) +	(dQ.q3 * r.q0);
	return Quat;
}
/**
 * schickt einen Quaternionen ueber usart raus
 * @deprecated
 */
void SendQuatUsart(dQuat dq){
	char puffer[80];
	sprintf(puffer,"$,%lf,%lf,%lf,%lf,#",dq.q0,dq.q1,dq.q2,dq.q3);
	USART_schreiben(puffer);

}

