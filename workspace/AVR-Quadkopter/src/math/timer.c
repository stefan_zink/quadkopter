#include "timer.h"
/*
 * Berechnet die verstrichene Zeit seit dem letzten Aufruf
 */

unsigned int delta;

unsigned int delta_t(timestruct *time){
	volatile unsigned int currenttime = getTime();
	delta = currenttime - time->time;
	time->time = currenttime;
	time->deltatime = delta;
	return delta;
 }

void init_time(timestruct *time){
	time->time = getTime();
	time->deltatime = 0;
}
