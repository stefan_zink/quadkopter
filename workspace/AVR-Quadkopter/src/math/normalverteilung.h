#ifndef NORMALVERTEILUNG_H_
#define NORMALVERTEILUNG_H_
#include "../sensors/sensormanagement.h"
#include "winkel.h"
#include "../basics/structs.h"


void initSensorNormalverteilung(sensorDaten_raw* sensorWerte_raw2,
		sensorDaten* sensorWerte2, kalibrierung* kalibrierungStruct2,
		winkelstruct* adx_winkelStruct2, winkelstruct* itg_winkelStruct2,
		timestruct* timeStruct2);
void varianz(int samples);
#endif
