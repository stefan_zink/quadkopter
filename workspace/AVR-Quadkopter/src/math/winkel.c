#include "winkel.h"

float dt = 0;


void initDrehwinkel(winkelstruct* winkel) {
	winkel->x_current = 0;
	winkel->x = 0;
	winkel->y_current = 0;
	winkel->y = 0;
	winkel->z_current = 0;
	winkel->z = 0;
}

/**
 * macht aus den winkelgeschwindigkeiten vom itg einen winkel in Grad
 *
 */
void berechneDrehwinkel(sensorDaten* sensorWerte, winkelstruct* winkel, unsigned int sample_time) {
	//timestruct* time
	//zeit von ms in s umrechnen
	//dt = delta_t(time);
	dt = (float)sample_time * 0.001f;
	//	double l=zeit * 0.001;//samplezeit to milisekunden
	winkel->x_current = (sensorWerte->gyro_x_bias * dt);
	winkel->x += winkel->x_current;// / 1000;
	winkel->y_current = (sensorWerte->gyro_y_bias * dt);
	winkel->y += winkel->y_current;// / 1000;
	winkel->z_current = (sensorWerte->gyro_z_bias * dt);
	winkel->z += winkel->z_current;// / 1000;

	//winkel >= 360 Grad machen keinen Sinn
	winkel->x = fmodf(winkel->x, 360.f);
	winkel->y = fmodf(winkel->y, 360.f);
	winkel->z = fmodf(winkel->z, 360.f);
}

void berechneBeschleunigungswinkel(sensorDaten* sensorWerte,
		winkelstruct* winkel) {
	double x = sensorWerte->acc_x_bias;
	double y = sensorWerte->acc_y_bias;
	double z = sensorWerte->acc_z_bias;

	// normierung - lauft hoffentlich auch ohne
	//	double totalforce = sqrt(x*x + y*y + z*z);
	//	x /= totalforce;
	//	y /= totalforce;
	//	z /= totalforce;

	winkel->x = atan2(x, z) * 360 / M_PI;
	winkel->y = atan2(y, z) * 360 / M_PI;
}
