/*
 * kalmmanfilter.h
 *	kalmanfilter methoden .c datei halt
 */

#ifndef KALMANFILTER_H_
#define KALMANFILTER_H_

#include "myMath.h"
#include "../basics/structs.h"
#include "../basics/basics.h"
#include <stdio.h>
#include "../basics/coreconfig.h"

#endif /* KALMMANFILTER_H_ */
/**
 * parameter
 */
//prozessrauschen

//void assignXk_1(double phi_acc,double omega_gryo,kalmanstruct* kalman);//weist X (k-1) zu
void assignYK_1(double phi_acc,double omega_gyro,kalmanstruct* kalman);//weist y (k-1) zu
void initMatrizen(int sample_time,kalmanstruct* kalman);
/**
 * Praediktionsgleichungen
 */
void Kalm_Prae_Calculate_X_K(double A[2][2],double Xk_1[2],double B[2][2],double Uk_1[2],double* R);

void Kalm_Prae_Calculate_P_K(double A[2][2],double Pk_1[2][2],double Qk[2][2],double R[2][2]);
/**
 * Hilfsmethoden fuer korrekturen
 */
void Kalm_Korr_Help_Residualkovarianz(double C[2][2],double P_k[2][2],double R[2][2],double res[2][2]);

void Kalm_Korr_Help_Innovation(double Yk[2], double C[2][2], double Xk[2], double res[2]);

/**
 * Korrektur
 */
void Kalm_Korr_Calculate_KK(double Pk[2][2],double C[2][2],double R[2][2],double res[2][2]);

void Kalm_Korr_Calculate_XK(double Xk[2], double KK[2][2], double Yk[2], double C[2][2], double res[2]);

void Kalm_Korr_Calculate_PK(double I[2][2], double KK[2][2], double C[2][2], double PK[2][2], double res[2][2]);
/**
 * Ausgabe
 */
void Kalm_view_to_Dip_XKYK(kalmanstruct* k);
void Kalm_view_to_Dip_KKPK(kalmanstruct* k);

void initKalman(base_config *core, kalmanstruct *x_kalmanStruct, kalmanstruct *y_kalmanStruct);
void __Kalmann(kalmanstruct *nick_achse, kalmanstruct *roll_achse);
void setKalmanValues(kalmanstruct *x_kalmanStruct, kalmanstruct *y_kalmanStruct, winkelstruct *adx_winkelStruct, sensorDaten *sensorWerte);

