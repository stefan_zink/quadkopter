/*
 * regelung.c
 *
 *  Created on: 13.03.2015
 *      Author: xxx
 */

#include "regelung.h"

/**
 * Regelungsstruct initialisieren
 */
void initRegelung(regelungspara *regelungsStruct) {
	// nick - x
	regelungsStruct->nick_aim = 0;
	// roll - y
	regelungsStruct->roll_aim = 0;
	// yaw - z
	regelungsStruct->yaw_aim = 0;
}

void updateRegelung(regelungspara *regelungsStruct,
		kalmanstruct *x_kalmanStruct, kalmanstruct *y_kalmanStruct) {
	regelungsStruct->nick_current = x_kalmanStruct->Xk[0];// mit kalmaahn
	regelungsStruct->roll_current = y_kalmanStruct->Xk[0];
}

void __Regelung(enginestruct *engineStruct, regelungspara *regelungsStruct,
		kalmanstruct *x_kalmanStruct, kalmanstruct *y_kalmanStruct,
		base_config *core) {
	xRegelung(engineStruct, regelungsStruct, x_kalmanStruct, core);
	yRegelung(engineStruct, regelungsStruct, y_kalmanStruct, core);
	setEngine(engineStruct);
}

void xRegelung(enginestruct *engineStruct, regelungspara *regelungsStruct,
		kalmanstruct *x_kalmanStruct, base_config *core) {
	engineStruct->StellWert2 = core->base_speed;
	engineStruct->StellWert4 = core->base_speed;
	//TODO: GIB'S MIR
}
void yRegelung(enginestruct *engineStruct, regelungspara *regelungsStruct,
		kalmanstruct *y_kalmanStruct, base_config *core) {
	double dt = core->sample_time * 0.001;

	double error_y = regelungsStruct->roll_aim - regelungsStruct->roll_current;
	// anwendung PID, Werte aus Skript UE4 (Hinweis zu Aufgabe 2)
	double stell_y = (1.5* error_y)  // P
//			+ (0.4 * ((error_y - regelungsStruct->e_y_alt) / dt))  // D
//			+ (0.1 * dt * regelungsStruct->e_y_sum)// I
			;

	// Werte uebertragen
	regelungsStruct->e_y_alt = error_y;
	regelungsStruct->e_y_sum += error_y;

	// engines einstellen
	engineStruct->StellWert3 = core->base_speed + stell_y;
	engineStruct->StellWert1 = core->base_speed - stell_y;
}
