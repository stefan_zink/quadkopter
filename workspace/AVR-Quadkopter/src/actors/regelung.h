/*
 * regelung.h
 *
 *  Created on: 13.03.2015
 *      Author: xxx
 */

#ifndef REGELUNG_H_
#define REGELUNG_H_
#include "../basics/structs.h"
#include "../basics/basics.h"
#include "../basics/coreconfig.h"
#include "engineControl.h"

void initRegelung(regelungspara *regelungsStruct);
void updateRegelung(regelungspara *regelungsStruct, kalmanstruct *x_kalmanStruct, kalmanstruct *y_kalmanStruct);

void __Regelung(enginestruct *engineStruct, regelungspara *regelungsStruct, kalmanstruct *x_kalmanStruct, kalmanstruct *y_kalmanStruct, base_config *core);

void xRegelung(enginestruct *engineStruct, regelungspara *regelungsStruct, kalmanstruct *x_kalmanStruct, base_config *core);
void yRegelung(enginestruct *engineStruct, regelungspara *regelungsStruct, kalmanstruct *y_kalmanStruct, base_config *core);
#endif /* REGELUNG_H_ */
