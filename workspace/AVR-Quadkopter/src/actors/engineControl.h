/*
 * enginCon.h
 *
 *  Created on: May 14, 2014
 *      Author: I8L-PC05-G01
 */
#ifndef ENGINCON_H_
#define ENGINCON_H_
#include <stdio.h>
#include <math.h>
#include "../basics/structs.h"
#include "../basics/basics.h"
#include "../basics/coreconfig.h"
#include "../avr-driver/AVR_Framework.h"
#include "BLCTRL_NG.h"

#endif /* ENGINCON_H_ */

void attitude_control(enginestruct* engin, regelungspara* param, int sampletime);
void yaw_control(enginestruct* engin, regelungspara* param, int sampletime);
void dip_view_engines(enginestruct* en);
void setEngine(enginestruct* en);

void regelung_angle_to_disp(regelungspara* param);
int isInStableFlight();
void setTrim(regelungspara* para);
