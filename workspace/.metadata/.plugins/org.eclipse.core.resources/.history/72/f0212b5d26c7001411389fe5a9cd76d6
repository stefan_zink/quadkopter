/*
 * ADXL345.c
 * Datei fuer den Accelerometer
 */
// ADXL Accelerometer
#include "../basics/basics.h"
#include "../avr-driver/AVR_Framework.h"
#include "../avr-driver/TWI_NG.h"
#include "ADXL345.h"
#include "../basics/structs.h"
#include <math.h>
#include "../avr-driver/TC_NG.h"
#define SHIFT 8

char data_received_adxl[RECEIVE_DATA_LENGTH]; // Contains Data after read

twi_package_t packet_adxl;

extern char debug_line[DEBUG_BUFFER_SIZE];

// Setup Value (datas) for ADXL to startup
const char setup_data_POWER_CTRL[1] = { 0x08 };
const char setup_data_DATA_FORMAT[1] = { 0x0B };
const char setup_data_BW_RATE[1] = { 0x0D };

/**
 * adx1345_read liest sensordaten aus udn schreibt se ins sensorDaten_raw struct
 */
void adxl345_read(sensorDaten_raw* sensorWerte_raw) {
	// Reads from TWI, writes to SensorDaten_raw_double Structure
	// Blocking Read
	// 2 Bytes, scaled

	// Read Twice, if reset was necessary
	twi_read(0, &packet_adxl);

	//short a=data_received_adxl[0]<<SHIFT;
	short a = data_received_adxl[0];
	short b = data_received_adxl[1] << SHIFT;
	short X = a | b;
	//short c=data_received_adxl[2]<<SHIFT;
	short c = data_received_adxl[2];
	short d = data_received_adxl[3] << SHIFT;
	short Y = c | d;
	//short e=data_received_adxl[4]<<SHIFT;
	short e = data_received_adxl[4];
	short f = data_received_adxl[5] << SHIFT;
	short Z = e | f;//SHOOORT
	sensorWerte_raw->aX = Y;
	sensorWerte_raw->aY = X;
	sensorWerte_raw->aZ = Z;

}
/**
 * adx1345_toDISP_raw zeigt ein sensorDaten_raw struct aufn display an
 */
void adx1345_toDISP_raw(sensorDaten_raw* sensorWerte_raw) {
	char puffer[20];
	dip204_clear_display();
	sprintf(puffer, "adx:X %d", sensorWerte_raw->aX);
	dip_write_string(puffer, 1);
	sprintf(puffer, "adx:y %d", sensorWerte_raw->aY);
	dip_write_string(puffer, 2);
	sprintf(puffer, "adx:Z %d", sensorWerte_raw->aZ);
	dip_write_string(puffer, 3);
	sprintf(puffer, "adx::rawww");
	dip_write_string(puffer, 4);
}
/**
 * adx1345_bias , fuert n angleich aus,
 * sensorDaten_raw		input		sensorDaten gehen rein
 * sensorDaten			output		sensorWerte die nun kalibriert sind und so
 * kalibrierung			putput		kalibrierungswerte
 */
void adx1345_bias(sensorDaten_raw* sensorWerte_raw, sensorDaten* sensorWerte,
		kalibrierung* kaliStruct) {
	//raw
	sensorWerte->acc_x = (double) sensorWerte_raw->aX - kaliStruct->acc_X;
	sensorWerte->acc_y = (double) sensorWerte_raw->aY - kaliStruct->acc_Y;
	sensorWerte->acc_z = (double) sensorWerte_raw->aZ - kaliStruct->acc_Z
			+ (double) (1000 / ADXL_SCALE_FACTOR);

	//conditioned
	sensorWerte->acc_x_bias = (double) sensorWerte_raw->aXcond
			- kaliStruct->acc_X_cond;
	sensorWerte->acc_y_bias = (double) sensorWerte_raw->aYcond
			- kaliStruct->acc_Y_cond;
	sensorWerte->acc_z_bias = (double) sensorWerte_raw->aZcond
			- kaliStruct->acc_Z_cond + 1000;
}

/**
 * Zeigt das zeug alles an
 */
void adx1345_toDisp_bias(sensorDaten* sensorWerte) {
	//USART_schreiben("schreibe adx kalib");
	char puffer[50];
	dip204_clear_display();
	sprintf(puffer, "adx:X %5.1lf %5.1lf", sensorWerte->acc_x,
			sensorWerte->acc_x_bias);
	dip_write_string(puffer, 1);
	sprintf(puffer, "adx:y %5.1lf %5.1lf", sensorWerte->acc_y,
			sensorWerte->acc_y_bias);
	dip_write_string(puffer, 2);
	sprintf(puffer, "adx:Z %5.1lf %5.1lf", sensorWerte->acc_z,
			sensorWerte->acc_z_bias);
	dip_write_string(puffer, 3);
	sprintf(puffer, "adx::Kalibrated");
	dip_write_string(puffer, 4);
}
/**
 * lese methode, wieoben
 */
int adxl345_read_ex(sensorDaten_raw* sensorWerte_raw) {
	// Reads from TWI, writes to SensorDaten_raw_double Structure
	// Non blocking Read
	// 2 Bytes, scaled

	// If Read Error					To detect if there is no sensor, return error
	if (twi_read_ex(0, &packet_adxl)) {
		return 1; // Error
	}

	// Read Twice, if reset was necessary
	twi_read(0, &packet_adxl);
	adxl345_read(sensorWerte_raw);
	// Compose bytes here
	// !!!!!!
	//KK

	return 0;
}

/**
 * 	Konditioniert die Werte, braucht roh werte, liefert conditionierte werte
 */
void adxl345_cond(sensorDaten_raw* sensorWerte_raw) {
	float scale = ADXL_SCALE_FACTOR;
	sensorWerte_raw->aXcond = ((float) sensorWerte_raw->aX) * scale;
	sensorWerte_raw->aYcond = ((float) sensorWerte_raw->aY) * scale;
	sensorWerte_raw->aZcond = ((float) sensorWerte_raw->aZ) * scale;
}

/**
 *	ermoeglicht das anzeigen der werte jener
 */
void adx1345_toDISP_cond(sensorDaten_raw* sensorWerte_raw) {
	char puffer[20];
	dip204_clear_display();
	sprintf(puffer, "adx:X %lf", sensorWerte_raw->aXcond);
	dip_write_string(puffer, 1);
	sprintf(puffer, "adx:y %lf", sensorWerte_raw->aYcond);
	dip_write_string(puffer, 2);
	sprintf(puffer, "adx:Z %lf", sensorWerte_raw->aZcond);
	dip_write_string(puffer, 3);
	sprintf(puffer, "adx::cond");
	dip_write_string(puffer, 4);
}
/**
 *	kalibrieungsmethode
 */
void adx1345_calib(kalibrierung* KaliKaliKali, int kalibrierungen) {
	//dip204_clear_display();
	dip_write_string("ADX Kali", 3);
	int i = 0;
	double x;
	double y;
	double z;
	sensorDaten_raw temp;
	for (i = 0; i < kalibrierungen; i++) {
		adxl345_read(&temp);
		x += (temp.aX);
		y += (temp.aY);//casen is iwie strange mal mit mal ohne klammern lelvoid itg3200Kali_toDISP(kalibrierung* kalikalikali);
		z += (temp.aZ);
		wait_ms(5);
	}

	x /= kalibrierungen;
	y /= kalibrierungen;
	z /= kalibrierungen;
	KaliKaliKali->acc_X = x;
	KaliKaliKali->acc_Y = y;
	KaliKaliKali->acc_Z = z;
	//und dieswerte nun konditionieren
	adxl345_cond(&temp);
	KaliKaliKali->acc_X_cond = temp.aXcond;
	KaliKaliKali->acc_Y_cond = temp.aYcond;
	KaliKaliKali->acc_Z_cond = temp.aZcond;

	dip_write_string("...DONE", 4);
	wait_ms(1000);
}
/**
 * zeigt die kalibrierungs roh daten an
 */
void adx1345Kali_toDisp_raw(kalibrierung* kalikalikali) {
	char puffer[20];
	dip204_clear_display();
	sprintf(puffer, "adx:X %d", kalikalikali->acc_X);
	dip_write_string(puffer, 1);
	sprintf(puffer, "adx:y %d", kalikalikali->acc_Y);
	dip_write_string(puffer, 2);
	sprintf(puffer, "adx:Z %d", kalikalikali->acc_Z);
	dip_write_string(puffer, 3);
	sprintf(puffer, "adx::kali::raw");
	dip_write_string(puffer, 4);
}
/**
 * zeigt kalibrierungs daten konditioniert an
 */
void adx1345Kali_toDisp_cond(kalibrierung* kalikalikali) {
	char puffer[20];
	dip204_clear_display();
	sprintf(puffer, "adx:X %lf", kalikalikali->acc_X_cond);
	dip_write_string(puffer, 1);
	sprintf(puffer, "adx:y %lf", kalikalikali->acc_Y_cond);
	dip_write_string(puffer, 2);
	sprintf(puffer, "adx:Z %lf", kalikalikali->acc_Z_cond);
	dip_write_string(puffer, 3);
	sprintf(puffer, "adx::kali::raw");
	dip_write_string(puffer, 4);
}
/**
 * macht aus beschleunigungen einen winkel
 */
void adx1345_to_angle(sensorDaten* sensorWerte, winkelstruct* winkl) {
	double x = (sensorWerte->acc_x_bias / sensorWerte->acc_z_bias);//funfact stiwched x and y
	double y = (sensorWerte->acc_y_bias / sensorWerte->acc_z_bias);
	//double z=(sensorWerte->acc_x_bias / sensorWerte->acc_y_bias) ;// is mir egal der wert//TODO mal sin
	//gemessene g wird nun in winkel umgerechnet
	x = atan(x);
	y = atan(y);
	//z=atan(z);
	//asiniert
	//und nun in grad umgerechnet
	x /= 2*PI;
	y /= 2*PI;
	//z/=2*PI;
	x *= -360;
	y *= 360;
	//y*=-1;
	//z*=360;
	//z*=-1;
	//y+=90;
	//z*=0;
	winkl->x_current = x;
	winkl->y_current = y;
	//winkl->z_current=z;
}
/**
 *	zeigt n winkel aufn dip an
 */
void adx1345_to_angle_disp(winkelstruct* winkl) {

	char puffer[20];
	dip204_clear_display();
	sprintf(puffer, "adx:X %lf", winkl->x_current);
	dip_write_string(puffer, 1);
	sprintf(puffer, "adx:y %lf", winkl->y_current);
	dip_write_string(puffer, 2);
	sprintf(puffer, "adx:Z %lf", winkl->z_current);
	dip_write_string(puffer, 3);
	sprintf(puffer, "adx::angle::");
	dip_write_string(puffer, 4);
}
/**
 * init..
 */
int adxl345_init(void) {
	// Setup the TWI packages

	int status = 0; // for debug, return USART error

	// --------- 	Setup ADXL345	--------------------------------------
	// Set POWER_CTRL Register
	// TWI chip address to communicate with
	packet_adxl.chip = ADXL_TWI_ADDRESS;
	// TWI address/commands to issue to the other chip (node)
	packet_adxl.addr = ADXL_POWER_CTRL_ADR;
	// Length of the TWI data address segment (1-3 bytes)
	packet_adxl.addr_length = EEPROM_ADDR_LGT;
	// Where to find the data to be written
	packet_adxl.buffer = (void*) setup_data_POWER_CTRL;
	// How many bytes do we want to write
	packet_adxl.length = 1;

	// Init ADXL, Set Power On
	//while (status != TWI_SUCCESS)
	status += twi_master_write_ex_edit(&AVR32_TWI, &packet_adxl);

	// Set DATA_FORMAT Register
	// TWI chip address to communicate with
	packet_adxl.chip = ADXL_TWI_ADDRESS;
	// TWI address/commands to issue to the other chip (node)
	packet_adxl.addr = ADXL_DATA_FORMAT_ADR;
	// Length of the TWI data address segment (1-3 bytes)
	packet_adxl.addr_length = EEPROM_ADDR_LGT;
	// Where to find the data to be written
	packet_adxl.buffer = (void*) setup_data_DATA_FORMAT;
	// How many bytes do we want to write
	packet_adxl.length = 1;

	// Set ADXL Data Format
	//while (status != TWI_SUCCESS)
	status += twi_master_write_ex_edit(&AVR32_TWI, &packet_adxl);

	// Set BW_RATE Register (Sample Rate)
	// TWI chip address to communicate with
	packet_adxl.chip = ADXL_TWI_ADDRESS;
	// TWI address/commands to issue to the other chip (node)
	packet_adxl.addr = ADXL_BW_RATE_ADR;
	// Length of the TWI data address segment (1-3 bytes)
	packet_adxl.addr_length = EEPROM_ADDR_LGT;
	// Where to find the data to be written
	packet_adxl.buffer = (void*) setup_data_BW_RATE;
	// How many bytes do we want to write
	packet_adxl.length = 1;

	// Set ADXL Data Format
	//while (status != TWI_SUCCESS)
	status += twi_master_write_ex_edit(&AVR32_TWI, &packet_adxl);

	// TWI chip address to communicate with
	packet_adxl.chip = ADXL_TWI_ADDRESS;
	// Length of the TWI data address segment (1-3 bytes)
	packet_adxl.addr_length = EEPROM_ADDR_LGT;
	// How many bytes do we want to write
	packet_adxl.length = RECEIVE_DATA_LENGTH;
	// TWI address/commands to issue to the other chip (node)
	packet_adxl.addr = ADXL_SENSING_ADDR_START;
	// Where to find the data to be written
	packet_adxl.buffer = (void*) data_received_adxl;

	return status;

	// ------------------------------------------------------------------
}
