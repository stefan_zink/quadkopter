/*
 * quad_frame.h
 *
 */

#ifndef QUAD_FRAME_H_
#define QUAD_FRAME_H_
#define LED0_GPIO AVR32_PIN_PB27
#define LED1_GPIO AVR32_PIN_PB28
#define LED2_GPIO AVR32_PIN_PB29
#define LED3_GPIO AVR32_PIN_PB30
#define LED4_GPIO AVR32_PIN_PB19
#define LED5_GPIO AVR32_PIN_PB20
#define LED6_GPIO AVR32_PIN_PB21
#define LED7_GPIO AVR32_PIN_PB22

#endif /* QUAD_FRAME_H_ */
