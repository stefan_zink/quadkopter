/********************************************************
 Name          : main.c
 Author        : Nils Gageik
 Copyright     : Not really
 Description   : LR Labor Frame
 **********************************************************/

/*! \page License
 * Copyright (c) 2009 Atmel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an Atmel
 * AVR product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
 *
 */
#include "AVR_Framework.h"
#include "basics.h"
#include "ADXL345.h"
#include "ITG3200.h"
#include "TWI_NG.h"
#include "TC_NG.h"
#include "usart.h"
#include "power_clocks_lib.h"
#include <stdio.h>
#include "structs.h"
#include "quaternion.h"
#include "myMath.h"
#include "kalmanfilter.h"
#include "BLCTRL_NG.h"
#include "enginCon.h"
#include "ring_puffer.h"
#include "quad_frame.h"
#include "coreconfig.h"
#include "route_calc.h"

#define USART0               (&AVR32_USART0)
#define USART_IRQ             AVR32_USART0_IRQ
#define pagezahl 24

static void initUsart();
static void initDIP();
static void initTC();
static void initSensoric();

__attribute__((__interrupt__))static void Joy_int_handler(void);
void dip204_configure_controlls(void);
__attribute__((__interrupt__))static void dip204_PB_handler(void);

void validatePage();
void WriteOnDisp();
void assignPages();
void writeHelpPage();
void validateHoriz();
void ONnOffDisp();
void __registerINTC();
void __Quaterionen();
void __Messung();
void __Kalmann();
void __Regelung();
void __UART_3DOF();
void __enroute();
void initS_route();
void reassingTC();
void refreshDebug();
void dip_view_buttonic();
void setLED(int led,int num);

unsigned char debug_csv_=0;
unsigned int tc_count=0;
unsigned int tc_old=0;
unsigned char PB[3]={0,0,0};
unsigned char PB_b[3]={0,1,1};//behaviour, if clear on repress
unsigned char current_node=0;
unsigned char rout_mode=0;

extern unsigned int tc_ticks;//timercounter referen
extern char engine_on;//referenz auf motoren an
extern char Engin_Watch_doge[20];//referenz auf fehler in methode
extern ringpuffer ringpuffer_A;
extern rout routs[max_knots-1];
extern unsigned char r_knot;

winkel itg_angle;//itg winkel
winkel adx_angle;//adx winkel
winkel kalman_angle;//not really in use, nei not realay
sensorDaten_raw sensorWerte_raw;//sensordaten roh
sensorDaten sensorWerte;//sensordaten
dQuat quat_old;//quaternion
kalibrierung kalikali;//kalibrierungsdaten
Kalman_Achse nick_achse;//nick_achse
Kalman_Achse roll_achse;//roll_achse
dQuat quat_neu;//altes quaternion
Engines engin;//mortorensteuerdaten
regelungspara regelung;//regelungsparameter
base_config core;

int page_current = 0;//page
int horiz = 1;//DOKU disp/overview/focus
int todo = 0;//DOKU :: Display an!/isan/aus!/isaus
int fpt=0;//DOKU :: Refreshcount
char debug_line[DEBUG_BUFFER_SIZE];
char pages[pagezahl][20];


int main(void) {
	setLED(0,1);
	initUsart();
	initDIP();
	dip204_configure_controlls();
	__registerINTC();
	dip204_set_cursor_position(1,1);
	dip204_write_string("huhu");
	dip204_hide_cursor();
	initTC();
	sprintf(debug_line,"RC:%d::",RC);
	USART_schreiben(debug_line);
	USART_schreiben("huhu");
	initSensoric();
	initConfig(&core); //hier wird die config geladen
	PB_b[0]=0;	PB_b[1]=1;	PB_b[2]=1;
	dip204_clear_display();
	dip_write_string("please do not move",1);
	dip_write_string("due to current calibrating ",2);
	dip_write_string("in progress",3);
	adxl345_read(&sensorWerte_raw);
	itg3200_read(&sensorWerte_raw);
	itg3200_calib(&kalikali,core.kalibrierungsanzahl);
	adx1345_calib(&kalikali,core.kalibrierungsanzahl);
	assignPages();
	initMatrizen(core.sample_time,&nick_achse);
	initMatrizen(core.sample_time,&roll_achse);
	quat_old= RPY_to_Quaternion(0.0,0.0,0.0);
	nick_achse.ax='X';
	roll_achse.ax='Y';
	nick_achse.A[0][1]=core.sample_time*0.001;
	roll_achse.A[0][1]=core.sample_time*0.001;
	dip_write_string("Preparing",1);
	dip_write_string("Routing",2);
	initS_route();
	dip_write_string("..DONE",3);
	page_current=18;
	horiz=2;
	blctrl_init();
	regelung.nick_aim=0.0;
	regelung.roll_aim=0.0;
	regelung.yaw_aim=0.0;
	setLED(0,0);
	if(debug_csv_)USART_schreiben("T;A_Z;R_X;KalmX;ITGX;ADXX;KalmY;ITGY;ADXY;ITGZ;EN1;EN2;EN3;EN4;\n");
	char debug_csv[150];
	while (!PB[0])
	{
		if(tc_ticks>(tc_old+core.sample_time)){
				tc_count+=core.sample_time;
				tc_old=tc_ticks;
			__Messung();
			itg3200_winkel(&sensorWerte,&itg_angle,core.sample_time);
			adx1345_to_angle(&sensorWerte,&adx_angle);
				nick_achse.Yk[0]=adx_angle.x_current;
				nick_achse.Yk[1]=sensorWerte.gyro_x_bias;
				roll_achse.Yk[0]=adx_angle.y_current;
				roll_achse.Yk[1]=sensorWerte.gyro_y_bias;
			__Kalmann();
				regelung.nick_current=nick_achse.Xk[0];// mit kalmaahn
				regelung.roll_current=roll_achse.Xk[0];
				regelung.yaw_current=itg_angle.z;
			__UART_3DOF();
			__enroute();
			if(debug_csv_){
					sprintf(debug_csv,"%d;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf;%3.2lf\n",tc_count,sensorWerte.acc_z_bias,regelung.nick_current,nick_achse.Xk[0],itg_angle.x,adx_angle.x_current,roll_achse.Xk[0],itg_angle.y,adx_angle.y_current,itg_angle.z,engin.StellWert1,engin.StellWert2,engin.StellWert3,engin.StellWert4);
					USART_schreiben(debug_csv);
				}

			__Regelung();
			setEngine(&engin);
			//dip204_write_string("T");
			refreshDebug();
		}

	}
	dip204_clear_display();
	dip204_set_cursor_position(1,1);
	dip204_write_string("EXECUTION HALT");
	dip204_set_cursor_position(1,2);
	dip204_write_string(Engin_Watch_doge);
	return 0;

}
///////////////////////////////////////
//Initialisierungen und so 	         //
///////////////////////////////////////

///////////////////////////////////////
//Interrupts & INTC zeug   	         //
///////////////////////////////////////
void __registerINTC(){
		Disable_global_interrupt();
		INTC_init_interrupts();
		INTC_register_interrupt		(&Joy_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_JOYSTICK_UP/8) ,    AVR32_INTC_INT1);
		INTC_register_interrupt		(&Joy_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_JOYSTICK_DOWN/8) ,  AVR32_INTC_INT1);
		INTC_register_interrupt		(&Joy_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_JOYSTICK_PUSH/8),   AVR32_INTC_INT1);
		INTC_register_interrupt		(&Joy_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_JOYSTICK_LEFT/8),   AVR32_INTC_INT1);
		INTC_register_interrupt		(&Joy_int_handler, AVR32_GPIO_IRQ_0 + (GPIO_JOYSTICK_RIGHT/8),  AVR32_INTC_INT1);
//		dip204_configure_controlls();
		INTC_register_interrupt		(&usart_int_handler, AVR32_USART0_IRQ, AVR32_INTC_INT2);
		USART0->ier = AVR32_USART_IER_RXRDY_MASK;
		INTC_register_interrupt		( &dip204_PB_handler, AVR32_GPIO_IRQ_0 + (GPIO_PUSH_BUTTON_0/8), AVR32_INTC_INT1);
		INTC_register_interrupt		( &dip204_PB_handler, AVR32_GPIO_IRQ_0 + (GPIO_PUSH_BUTTON_1/8), AVR32_INTC_INT1);
		INTC_register_interrupt		( &dip204_PB_handler, AVR32_GPIO_IRQ_0 + (GPIO_PUSH_BUTTON_2/8), AVR32_INTC_INT1);
		Enable_global_interrupt();
}
__attribute__((__interrupt__))
static void dip204_PB_handler(void)
{
	if (gpio_get_pin_interrupt_flag(GPIO_PUSH_BUTTON_0))
	{
		if(PB_b[0]){
			PB[0]=!PB[0];
		}
		else PB[0]=true;
		gpio_clear_pin_interrupt_flag(GPIO_PUSH_BUTTON_0);
	}

	if (gpio_get_pin_interrupt_flag(GPIO_PUSH_BUTTON_1))
	{
		if(PB_b[1]){
			PB[1]=!PB[1];
			engine_on=PB[1];
		}
		else PB[1]=true;
		gpio_clear_pin_interrupt_flag(GPIO_PUSH_BUTTON_1);
	}

	if (gpio_get_pin_interrupt_flag(GPIO_PUSH_BUTTON_2))
	{
		if(PB_b[2]){
			PB[2]=!PB[2];
		}
		else PB[2]=true;
		gpio_clear_pin_interrupt_flag(GPIO_PUSH_BUTTON_2);
	}
}
__attribute__((__interrupt__))
static void Joy_int_handler(void)
{
	if (gpio_get_pin_interrupt_flag(GPIO_JOYSTICK_UP))
	{
		page_current--;
		gpio_clear_pin_interrupt_flag(GPIO_JOYSTICK_UP);
	}
	if (gpio_get_pin_interrupt_flag(GPIO_JOYSTICK_DOWN))
	{
		page_current++;
		gpio_clear_pin_interrupt_flag(GPIO_JOYSTICK_DOWN);
	}
	if (gpio_get_pin_interrupt_flag(GPIO_JOYSTICK_PUSH))
	{
		gpio_clear_pin_interrupt_flag(GPIO_JOYSTICK_PUSH);
	}
	if (gpio_get_pin_interrupt_flag(GPIO_JOYSTICK_LEFT))
	{
		horiz--;
		gpio_clear_pin_interrupt_flag(GPIO_JOYSTICK_LEFT);
	}
	if (gpio_get_pin_interrupt_flag(GPIO_JOYSTICK_RIGHT))
	{
		horiz++;
		gpio_clear_pin_interrupt_flag(GPIO_JOYSTICK_RIGHT);
	}
}
///////////////////////////////////////
//Konfigurierungen & Initialisierung //
///////////////////////////////////////
void dip204_configure_controlls(void)
{
	gpio_enable_pin_interrupt(GPIO_JOYSTICK_UP , GPIO_FALLING_EDGE);
	gpio_enable_pin_interrupt(GPIO_JOYSTICK_DOWN , GPIO_FALLING_EDGE);
	gpio_enable_pin_interrupt(GPIO_JOYSTICK_PUSH , GPIO_FALLING_EDGE);
	gpio_enable_pin_interrupt(GPIO_JOYSTICK_RIGHT , GPIO_FALLING_EDGE);
	gpio_enable_pin_interrupt(GPIO_JOYSTICK_LEFT , GPIO_FALLING_EDGE);

	gpio_enable_pin_interrupt(GPIO_PUSH_BUTTON_0 , GPIO_RISING_EDGE);
	gpio_enable_pin_interrupt(GPIO_PUSH_BUTTON_1 , GPIO_RISING_EDGE);
	gpio_enable_pin_interrupt(GPIO_PUSH_BUTTON_2, GPIO_RISING_EDGE);

}
void assignPages()
{	//seite 1
	sprintf(pages[0],"ADX RAW");
	sprintf(pages[1],"ADX COND");
	sprintf(pages[2],"ADX Kalibriert");
	//seite 2
	sprintf(pages[3],"ITG RAW");
	sprintf(pages[4],"ITG COND");
	sprintf(pages[5],"ITG Kalibriert");
	//seite 3
	sprintf(pages[6],"ADX Kali Raw");
	sprintf(pages[7],"ADX  Kali Cond");
	sprintf(pages[8],"QuaternionView");
	//seite 4
	sprintf(pages[9],"ITG Kali Raw");
	sprintf(pages[10],"ITG Kali Cond");
	sprintf(pages[11],"ITG Winkel");
	//seite 5
	sprintf(pages[12],"ADX phi");
	sprintf(pages[13],"TC::");
	sprintf(pages[14],"Kalman KKPK Nick");
	//seite 6
	sprintf(pages[15],"Kalman KKPK Roll");
	sprintf(pages[16],"Kalman YKXK Nick");
	sprintf(pages[17],"Kalman YKXK Roll");
	//seite 7
	sprintf(pages[18],"Regelung Winkel");
	sprintf(pages[19],"Button Beleg");
	sprintf(pages[20],"Engin Veloc");
	//seite 8
	sprintf(pages[21],"Ringpuffer");
	sprintf(pages[22],"--");
	sprintf(pages[23],"--");

}
void initS_route(){
	addKnot(0,0);
	addKnot(3,4);
	addKnot(8,4);
	addKnot(5,0);
	addKnot(0,0);
	ParseRouts(core.sample_time); //berechnet dir route
	int current_node;
	for(current_node=0;current_node<r_knot;current_node++){
		PrintRout(&routs[current_node],current_node);
		regelungspara temp;
			temp.yaw_current=0;
			temp.nick_aim=0;
			temp.roll_aim=0;
		CalCulateSuperPostion(&routs[current_node],&temp);
			char debug_[30];
			sprintf(debug_,"ACCN:%lf R:%lf",temp.nick_aim,temp.roll_aim);
			sprintf(debug_,"BRKN:%lf R:%lf",(-temp.nick_aim),(-temp.roll_aim));
			USART_schreiben(debug_);
	}
}
static void initUsart(){
	static const gpio_map_t USART_GPIO_MAP =  {
			{AVR32_USART0_RXD_0_0_PIN, AVR32_USART0_RXD_0_0_FUNCTION},
			{AVR32_USART0_TXD_0_0_PIN, AVR32_USART0_TXD_0_0_FUNCTION}
	};
	// USART options
	static const usart_options_t USART_OPTIONS =  {
			.baudrate     = 57600,
			.charlength   = 8,
			.paritytype   = USART_NO_PARITY,
			.stopbits     = USART_1_STOPBIT,
			.channelmode  = USART_NORMAL_CHMODE
	};

	pcl_switch_to_osc(PCL_OSC0, FOSC0, OSC0_STARTUP); // Clock Osc0 konfigurieren mit Startup Time

	gpio_clr_gpio_pin(AVR32_PIN_PB19);
	gpio_clr_gpio_pin(AVR32_PIN_PB20);
	gpio_set_gpio_pin(AVR32_PIN_PB21);
	gpio_set_gpio_pin(AVR32_PIN_PB22);

	// Assign GPIO to USART
	gpio_enable_module(USART_GPIO_MAP,  sizeof(USART_GPIO_MAP) / sizeof(USART_GPIO_MAP[0]));

	// Initialize USART in RS232 mode.
	usart_init_rs232(USART0 , &USART_OPTIONS, FOSC0);
}
static void initDIP(){
	// Map SPI Pins
	static const gpio_map_t DIP204_SPI_GPIO_MAP =
		{
				{DIP204_SPI_SCK_PIN,  DIP204_SPI_SCK_FUNCTION },  // SPI Clock.
				{DIP204_SPI_MISO_PIN, DIP204_SPI_MISO_FUNCTION},  // MISO.
				{DIP204_SPI_MOSI_PIN, DIP204_SPI_MOSI_FUNCTION},  // MOSI.
				{DIP204_SPI_NPCS_PIN, DIP204_SPI_NPCS_FUNCTION}   // Chip Select NPCS.
		};

		// Switch the CPU main clock to oscillator 0
		pcl_switch_to_osc(PCL_OSC0, CPU_SPEED, OSC0_STARTUP); // Switch main clock to external oscillator 0 (crystal), 12 MHz


		// add the spi options driver structure for the LCD DIP204
		spi_options_t spiOptions =
		{
				.reg          = DIP204_SPI_NPCS,
				.baudrate     = 1000000,
				.bits         = 8,
				.spck_delay   = 0,
				.trans_delay  = 0,
				.stay_act     = 1,
				.spi_mode     = 0,
				.modfdis      = 1
		};
		// SPI Inits: Assign I/Os to SPI
		gpio_enable_module(DIP204_SPI_GPIO_MAP,
				sizeof(DIP204_SPI_GPIO_MAP) / sizeof(DIP204_SPI_GPIO_MAP[0]));
		// Initialize as master
		spi_initMaster(DIP204_SPI, &spiOptions);
		// Set selection mode: variable_ps, pcs_decode, delay
		spi_selectionMode(DIP204_SPI, 0, 0, 0);
		// Enable SPI
		spi_enable(DIP204_SPI);
		// setup chip registers
		spi_setupChipReg(DIP204_SPI, &spiOptions, FOSC0);
		// initialize delay driver
		delay_init( FOSC0 );
		// initialize LCD
		dip204_init(backlight_PWM, TRUE);
}
static void initTC(){
	myTC_init();
}
static void initSensoric(){
	myTWI_init();//TWI treiber
	adxl345_init();//adxd
	itg3200_init();//itg
	USART_schreiben("Sensorik Initialisiert\n");
}


//////////////////////////////////////
//Ausfuehrungen 					//
//////////////////////////////////////
void refreshDebug(){
	if(fpt>core.fpt_limit){
		WriteOnDisp();
		fpt=0;
	}
	else fpt++;
}

void __Regelung(){
	attitude_control(&engin,&regelung,core.sample_time);
	yaw_control(&engin,&regelung,core.sample_time);
	//if(isInStableFlight()>0){ }

}
void __Quaterionen(){
	quat_neu=RPY_to_Quaternion(itg_angle.x_current,itg_angle.y_current,itg_angle.z_current);
	normiereQuaternion(&quat_neu);
	quat_old=quaternionenmultiplikation(quat_old,quat_neu);
	normiereQuaternion(&quat_old);
	SendQuatUsart(quat_old);
}

void __UART_3DOF(){
	while(isAvailable(&ringpuffer_A)){

		switch(read_from_buffer()){
			//pitch mit achse 1 und 3
			case '8':
				regelung.nick_aim--;
			break;
			case '2':
				regelung.nick_aim++;
			break;
			//roll mit achse 2 und 4
			case '4':
				regelung.roll_aim++;
			break;
			case '6':
				regelung.roll_aim--;
			break;
			case '7':
				regelung.yaw_aim++;
			break;
			case '9':
				regelung.yaw_aim--;
			break;
			case '5':
				regelung.roll_aim=0;
				regelung.nick_aim=0;
				regelung.yaw_aim=0;
			break;
			case 'S':
			case 's'://lel not case sensitive
				PB[2]=!PB[2];//ON PB 1 rout
			break;
			case '_':
				current_node=0;//setzt das routing zurueckt auf die erste route so dass nochmal abgefolgen werden kann
			break;
			case 'T': //so kann man neuen 0punkt setzen.
				setTrim(&regelung);
			break;
			case '?':
				PB[0]=1;
				sprintf(Engin_Watch_doge,"USART_OFF");
			break;
			case ':':
				PB[1]=!PB[2];
				engine_on=!engine_on;
			break;

		}
	}
}

void __enroute(){
	setLED(7,0);
	if(PB[2]){
		setLED(7,1);
		regelungspara temp;
		temp.yaw_current=0;
		temp.nick_aim=0;
		temp.roll_aim=0;

		//accen phasen
		//phase 0 -> anglen
		//phase 1 -> schauen das die richtung iwie n bissl stimm halbwegs, ansonsten wartn
		//routing phasen //sample gesteuert!//lel wird nemmer gnutz
		//phase 2 -> accelerieren,
		if(rout_mode==0){//muss anglen!
			regelung.nick_aim=0;
			regelung.roll_aim=0;
			regelung.yaw_aim=0;
			rout_mode+=2;
		}

		//regelung.yaw_current=0;
		//regelung.yaw_weight=0.0;
		//regelung.yaw_aim=routs[current_node].angle;
		//beschleunigen
		if(routs[current_node].sample_current_flight<routs[current_node].sample_till_flightFloat){
			setLED(4,0);
			//CalCulateSuperPostion(&routs[current_node],&regelung);
			CalCulateSuperPostion(&routs[current_node],&temp);
			setLED(1,1);
		}
		//fliegen
		else if(routs[current_node].sample_current_flight<routs[current_node].sample_till_breakFlight){
			setLED(1,0);
			temp.nick_aim=0;
			temp.roll_aim=0;
			setLED(2,1);
		}
		//bremsen
		else if(routs[current_node].sample_current_flight<routs[current_node].sample_till_stop){
			setLED(2,0);
			setLED(3,1);
			//CalCulateSuperPostion(&routs[current_node],&regelung);
			CalCulateSuperPostion(&routs[current_node],&temp);
			temp.nick_aim*=-1;
			temp.roll_aim*=-1;
		}
		//ideln
		else if(routs[current_node].sample_till_stop+idle_time_flight>routs[current_node].sample_current_flight){
			setLED(3,0);
			setLED(4,1);
			temp.nick_aim=0;
			temp.roll_aim=0;
		}

		if(rout_mode>1)routs[current_node].sample_current_flight++;//bin nun in routing phase

		if(routs[current_node].sample_till_stop+idle_time_flight<routs[current_node].sample_current_flight){
			routs[current_node].sample_current_flight=0;//setze route zurueck nach gebrauch
			rout_mode=0;
			if(current_node<r_knot){
				current_node++;
			}
			else {

				current_node=0;
				PB[2]=!PB[2];

			}
		}
		//weitergabe damit flieg flieg
		regelung.nick_aim=temp.nick_aim;
		regelung.roll_aim=temp.roll_aim;
		//temp weil sonst yaw mitmacht
	}
}

void __Messung(){

	adxl345_read(&sensorWerte_raw);
	itg3200_read(&sensorWerte_raw);

	adxl345_cond(&sensorWerte_raw);
	itg3200_cond(&sensorWerte_raw);

	adx1345_bias(&sensorWerte_raw,&sensorWerte,&kalikali);//bias is des teil iwie wo die kalibrierung drauffaellt
	itg3200_bias(&sensorWerte_raw,&sensorWerte,&kalikali);//bias
}
void __Kalmann(){
	// X'K = A * xk(alt);
	Kalm_Prae_Calculate_X_K(nick_achse.A,nick_achse.Xk,nick_achse.B,nick_achse.Uk_1,nick_achse.X_k);
	Kalm_Prae_Calculate_X_K(roll_achse.A,roll_achse.Xk,roll_achse.B,roll_achse.Uk_1,roll_achse.X_k);
	// P'k=A*Pk_1*A*Qk
	Kalm_Prae_Calculate_P_K(nick_achse.A,nick_achse.Pk,nick_achse.Qk,nick_achse.P_k);
	Kalm_Prae_Calculate_P_K(roll_achse.A,roll_achse.Pk,roll_achse.Qk,roll_achse.P_k);
	//Kk= P'k * C'T  * ( C * P'K *Ct + Rk)^-1
	Kalm_Korr_Calculate_KK(nick_achse.P_k,nick_achse.C,nick_achse.Rk,nick_achse.Kk);
	Kalm_Korr_Calculate_KK(roll_achse.P_k,roll_achse.C,roll_achse.Rk,roll_achse.Kk);
	// XK= x'k (siehe oben...) + KK * YK -C * x'k
	Kalm_Korr_Calculate_XK(nick_achse.X_k,nick_achse.Kk,nick_achse.Yk,nick_achse.C,nick_achse.Xk);
	Kalm_Korr_Calculate_XK(roll_achse.X_k,roll_achse.Kk,roll_achse.Yk,roll_achse.C,roll_achse.Xk);
	//Pk.. siehe folien...
	Kalm_Korr_Calculate_PK(nick_achse.I,nick_achse.Kk,nick_achse.C,nick_achse.P_k,nick_achse.Pk);
	Kalm_Korr_Calculate_PK(roll_achse.I,roll_achse.Kk,roll_achse.C,roll_achse.P_k,roll_achse.Pk);
}

void USART_schreiben(char* string){
	usart_write_line(USART0,string);
}
void WriteOnDisp(){
	dip204_clear_display();
	validatePage();
	validateHoriz();//kann eig theoretisch danach immernoch eintreten aber naja , was solls

	if(horiz==0);//display not in use
	else if(horiz==1)writeHelpPage(page_current);
	else if(horiz==2){
		switch(page_current){
		//Seite 1 : 0,1,2
		case 0:adx1345_toDISP_raw(&sensorWerte_raw);break;
		case 1:adx1345_toDISP_cond(&sensorWerte_raw);break;
		case 2:adx1345_toDisp_bias(&sensorWerte);break;
		//Seite 2 : 3,4,5
		case 3:itg3200_toDISP_raw(&sensorWerte_raw);break;
		case 4:itg3200_toDISP_cond(&sensorWerte_raw);break;
		case 5:itg3200_toDISP_bias(&sensorWerte);break;
		//Seite 3 : 6,7,8
		case 6:adx1345Kali_toDisp_raw(&kalikali);break;
		case 7:adx1345Kali_toDisp_cond(&kalikali);break;
		case 8:Disp_Quat(&quat_neu);break;
		//seite 4_ 9,10,11
		case 9:itg3200Kali_toDISP_raw(&kalikali);break;
		case 10:itg3200Kali_toDISP_cond(&kalikali);break;
		case 11:itg3200_winkel_toDISP(&itg_angle);break;
		//seite 5 ,12,13,14
		case 12: adx1345_to_angle_disp(&adx_angle);break;
		case 13: TC_DISP_TIME(tc_old,core.sample_time); break;
		case 14: Kalm_view_to_Dip_KKPK(&nick_achse);break;
		//seite 6 15,16,17
		case 15: Kalm_view_to_Dip_KKPK(&roll_achse);break;
		case 16: Kalm_view_to_Dip_XKYK(&nick_achse);break;
		case 17: Kalm_view_to_Dip_XKYK(&roll_achse);break;
		//seite 7
		case 18:regelung_angle_to_disp(&regelung);break;
		case 19:dip_view_buttonic();break;
		case 20:dip_view_engines(&engin);break;
		//seite 8
		case 21:Rpuffer_toDISP_raw(&ringpuffer_A);break;
		}

	}

}
void writeHelpPage(int counter){

	validatePage();
	int l=counter-counter%3;//warhscheinlich waere en einfache durch 3 addition auch gegangen -.-
	int x=0;
	//darstellen von bis zu 3 methoden pro seite
	int h;
	char minipuff[20];
	for(x=0;x<3;x++){
		//sprintf(minipuff,"x : %d ...",x);
		//USART_schreiben(minipuff);
		h=x+l;
		if(x<pagezahl){
			//if(h==counter)sprintf(minipuff,">%d %s",x,pages[h]);
			//else sprintf(minipuff,"%d %s",x,pages[h]);
			if(h==counter)sprintf(minipuff,">%d %s",h,pages[h]);
			else sprintf(minipuff,"%d %s",h,pages[h]);
			dip_write_string(minipuff,x+1);
		}
	}
}

void validatePage(){

		if(page_current < 0)
			page_current = 0;

		if(page_current >= pagezahl)
			page_current = pagezahl-1;//0 basiert ...

}

void validateHoriz(){
	//0 is aus, 1 is uebersicht, 2 is ausgewaehlt
	if(horiz<=0){
		horiz=0;
		if(todo>=0)todo=1;//disp is iwie an oder soll ausgeschalten werden, schalt lieber mal aus
	}
	else{
		if(todo==2)todo=3;//disp is aus, schalt mal an
	}
	if(horiz>2){
		horiz=2;
	}
	ONnOffDisp();
}
void ONnOffDisp(){
	if(todo==1){//schalts licht aus
		//dip204_set_backlight(0);//mal ausprobieren am montag
		dip204_init(backlight_PWM, FALSE);
		todo=2;//sei fertig
	}
	if(todo==3){//schalts licht an
		//dip204_set_backlight(1);//mal ausporbieren am montag
		dip204_init(backlight_PWM, TRUE);
		todo=0;
	}
	dip204_hide_cursor();

}
void dip_write_string(char* string, int zeile){
	dip204_set_cursor_position(1,zeile);
	dip204_write_string(string);
}
void dip_view_buttonic(){
	char yes[1];sprintf(yes,"Y");
	char no[1];sprintf(no,"N");
	int i=10;
	for(i=0;i<3;i++){

		dip204_set_cursor_position(3+(i*2),1);

		if(PB_b[i])dip204_write_string(yes);
		else dip204_write_string(no);


		dip204_set_cursor_position(3+(i*2),2);
		if(PB[i])dip204_write_string(yes);
		else dip204_write_string(no);
	}

}
/*
 * led nummer
 * num, mode
 * 	0 aus
 *  1 an
 *  2 toggle
 */
void setLED(int led,int num){

	void (*gpio_dosth)(unsigned)=&gpio_clr_gpio_pin;
	switch(num){
	case 0:
		gpio_dosth=&gpio_set_gpio_pin;//schaltet led aus
	break;
	case 1:
		gpio_dosth=&gpio_clr_gpio_pin;//schaltet led ein
	break;
	case 2:
		gpio_dosth=&gpio_tgl_gpio_pin;//toggled led
	break;
	}
	switch(led){
	/**
	 * ersten vier,green only
	 */
	case 1:
		gpio_dosth(LED0_GPIO);
	break;
	case 2:
		gpio_dosth(LED1_GPIO);
	break;
	case 3:
		gpio_dosth(LED2_GPIO);
	break;
	case 4:
		gpio_dosth(LED3_GPIO);
	break;
	case 7:
		gpio_dosth(LED6_GPIO);
	break;
	}
}
