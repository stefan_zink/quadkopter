/*
 * USART_REMOTE.h
 *
 *  Created on: 09.08.2011
 *      Author: gageik
 */
// USART FOR REMOTE CONTROL (SATELIT RECEIVER)
// Signal from Satelite is USART, with new Signal every 20-23ms
// It consists of 16Byte, 115200baud
// Reading time: 7x200 us = 1,4ms for 140bit, 10us = 1 bit



#ifndef USART_REMOTE_H_
#define USART_REMOTE_H_

// For Remote (Satelliten Receiver (Funkfernsteuerung))
#define USART_2               (&AVR32_USART2)
#define USART_2_RX_PIN        AVR32_USART2_RXD_0_0_PIN
#define USART_2_RX_FUNCTION   AVR32_USART2_RXD_0_0_FUNCTION
//#define USART_2_RX_PIN        AVR32_USART2_RXD_0_1_PIN
//#define USART_2_RX_FUNCTION   AVR32_USART2_RXD_0_1_FUNCTION

#define USART_2_TX_PIN        AVR32_USART2_TXD_0_0_PIN
#define USART_2_TX_FUNCTION   AVR32_USART2_TXD_0_0_FUNCTION
#define USART_2_IRQ           AVR32_USART2_IRQ

#define USART2_BAUDRATE      	115200

#define REMOTE_TOLERANZ			5

#define REMOTE_START_BYTE_1		0x03
#define REMOTE_START_BYTE_2 	0x01

#define REMOTE_YAW_MIN			3241
#define REMOTE_YAW_MEAN			3584
#define REMOTE_YAW_MAX			3934
#define REMOTE_PITCH_MIN		2214
#define REMOTE_PITCH_MEAN		2555
#define REMOTE_PITCH_MAX		2909
#define REMOTE_ROLL_MIN			1189
#define REMOTE_ROLL_MEAN		1540
#define REMOTE_ROLL_MAX			1882
#define REMOTE_THROTTLE_MIN		169			//5290
#define REMOTE_THROTTLE_MAX		854			//5974
#define REMOTE_GEAR_HIGH		4266
#define REMOTE_GEAR_LOW			4950
#define REMOTE_AUX2_HIGH		6315		//6912, 6312
#define REMOTE_AUX2_LOW			6999		//6144, 6996
#define REMOTE_FLT_MODE_N		5974
#define REMOTE_FLT_MODE_1		5632
#define REMOTE_FLT_MODE_2		5290

#define REMOTE_SAMPLE_TIME		0.022f
#define REMOTE_TIME_OUT			900
#define REMOTE_INIT_TIME_OUT	300

#define MIN_ANGLE_SOLL			1.0f		// Minimal Angle Set Points mind be just unaccuracy

#define LOOPING_INACTIVE		0
#define LOOPING_POSITIVE_DIR	1
#define LOOPING_NEGATIVE_DIR	2
#define LOOPING_STICK_THRESH	100

#define REMOTE_BUFFER_SIZE		200
void myREMOTE_init(void);
void remote_calibrate(void);
void remote_parse(void);
short remote_decode(char newByte);
void activate_remote(void);
void get_remote(void);
void myREMOTE_reset(void);
void setRemote(void);
void Remote(void);
short check_borders(short min, short max, short value);


typedef struct {
	short roll;				//Roll: Rechter Kn�ppel Links+Rechts
	short throttle;			//Gas: Linker Kn�ppel Hoch+Runter
	short pitch;			//Nicken: Rechter Kn�ppel Hoch+Runter
	short yaw;				//Gieren: Linker Kn�ppel Links+Rechts
	short gear_switch;		// Gear-Schalter (links)
	short aux2_switch;		// Aux2-Schalter (rechts)
	short aux2_switch_raw;
	short flightmode_switch;// FLT Mode Switch with 3 stats: N,1,2

	char looping_roll;
	char looping_pitch;

	short roll_bias;
	short pitch_bias;
	short yaw_bias;
	short throttle_bias;

	double soll_angle_roll;
	double soll_angle_pitch;
	double soll_angle_yaw;
	double gas;

	char automation_state;

	char FRAME[14];				// 14 Bytes Nutzdaten

	short new_values;
	short state;

	short active;
	short calibrated;
} remoteDaten;

typedef struct {
	char buffer[REMOTE_BUFFER_SIZE];

	int buffer_index;			// Index, zeigt auf letzten geschriebenen Eintrag
	int read_index;			// Index, zeigt n�chsten zu lesenen Eintrag
} remoteBuffer;


#endif /* USART_REMOTE_H_ */
