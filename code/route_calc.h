/*
 * route_calc.h
 *
 *  Created on: Jun 16, 2014
 *      Author: I8L-PC05-G01
 */

#ifndef ROUTE_CALC_H_
#define ROUTE_CALC_H_
#define max_knots 257
void addKnot(int x,int y); 		 //fuege wegpunkte hinzu
void ParseRouts(int sampletime); //macht knoten zu routen
#define acc_angle_max 7.2f;
void CalCulateSuperPostion(rout* r,regelungspara* para);
void PrintRout(rout* r,int p);
#define idle_time_flight 25 //sample
#endif /* ROUTE_CALC_H_ */

