/*
 * enginCon.c
 *
 */
#include "stdio.h"
#include "structs.h"
#include "BLCTRL_NG.h"
#include "basics.h"
#include "math.h"
#include "AVR_Framework.h"
#include "coreconfig.h"

extern base_config core;
extern unsigned char PB[3];//unsigned char referenz auf die PUSH_BUTTONS
extern unsigned char engine_on;//referenz aufs externe motor is an zeug

#define steady_flight_reset -55
#define safetyenvelope 240//240 entspricht 94%
//#define EngineIdle 60//90 entspricht 40%
//#define EngineIdle 100.0f//90 entspricht 40%
#define min_value 7.0//mindest dreh zahl
#define err_count 50.0//anzahlder maximalregelungsanschreitungen
//#define yawLimit 40.0f
#define yawLimit 25.0f

char Engin_Watch_doge[20];
int steady_flight_samples=steady_flight_reset;

double angle_envelop=3.5;
void verifyStableFlightLevel(regelungspara* param);
void pre_cast(Engines* en);
void watchdoge(Engines* en);


void attitude_control(Engines* engin,regelungspara* param,int sampletime){
	double samplezeit=sampletime*0.001;
	double e_x=param->nick_aim-param->nick_current+param->trim_x;
	double e_y=param->roll_aim-param->roll_current+param->trim_y;
	//			P 				 D											  I
	double x= (core.Pa * e_x )	+	(core.Da * ((e_x-param->e_x_alt)/samplezeit)) + ( core.Ia * samplezeit * param->e_x_sum);
	double y= (core.Pa * e_y )	+	(core.Da * ((e_y-param->e_y_alt)/samplezeit)) + ( core.Ia * samplezeit * param->e_y_sum);
	param->e_x_alt=e_x;
	param->e_y_alt=e_y;
	param->e_x_sum+=e_x;
	param->e_y_sum+=e_y;

	engin->StellWert1=core.base_speed+x;
	engin->StellWert3=core.base_speed-x;

	engin->StellWert4=core.base_speed+y;
	engin->StellWert2=core.base_speed-y;//klingt komisch is aber so

	verifyStableFlightLevel(param); //schaut ob man einen stabilen flug hat
}

int isInStableFlight(){
	return steady_flight_samples;
}
//wenn er �ber 3,5 grad ist, dann funktioniert nur attitude. das wird mit 55 samples �berpr�ft
void verifyStableFlightLevel(regelungspara* param){
	if((fabs(param->e_x_alt) < angle_envelop) && engine_on && (fabs(param->e_y_alt) < angle_envelop)){
		steady_flight_samples++;
	}
	else if (steady_flight_samples >=-steady_flight_reset)	steady_flight_samples-=5;
	else steady_flight_samples=steady_flight_reset;
}
//hier ist superposition von yawcontrol
void yaw_control(Engines* engin,regelungspara* param,int sampletime){
	double samplezeit=sampletime*0.001;
	//engine 1+3 = gegen Uhrzeigersinn -> Left  Rotate
	//engine 2+4 = im    Uhrzeigersinn -> Right Rotate

	double e_z= param->yaw_aim-param->yaw_current;

	double z= (core.Py * e_z )	+	(core.Dy * ((e_z - param->e_z_alt)/samplezeit)) + ( core.Iy * samplezeit * param->e_z_sum);
	//z reinskalieren nach anderen ausschlaegen

//	param->yaw_weight=(1- 	((engin->StellWert1+engin->StellWert2+engin->StellWert3+engin->StellWert4-core.base_speed*2)/(155*4	)	) 	);//IS iwie konstant
	//winkelfehler 20
	param->yaw_weight=(1-	((fabs(param->e_x_alt)+fabs(param->e_y_alt))/10.0)	);
	if(param->yaw_weight<=0)param->yaw_weight=0;
	z=param->yaw_weight*z;
//	if(z>yawLimit)z=yawLimit;
//	if(z<-yawLimit)z=-yawLimit;
	if(engine_on)param->e_z_alt=e_z;
	if(engine_on)param->e_z_sum+=e_z;
	engin->StellWert1-=z;
	engin->StellWert3-=z;
	engin->StellWert2+=z;
	engin->StellWert4+=z;
}
void regelung_angle_to_disp(regelungspara* param){
	char puffer[40];
			dip204_clear_display();
			sprintf(puffer,"reg:X %.6lf",param->nick_current);
				dip_write_string(puffer,1);
			sprintf(puffer,"reg:y %.6lf",param->roll_current);
				dip_write_string(puffer,2);
			sprintf(puffer,"reg:Z %.6lf",param->yaw_current);
				dip_write_string(puffer,3);
			sprintf(puffer,"YawW:%.6lf",param->yaw_weight);
				dip_write_string(puffer,4);
//			if(steady_flight_samples>0)sprintf(puffer,"reg::YAW::online");
//			else sprintf(puffer,"reg::YAW::offline");
//				dip_write_string(puffer,4);
}
//der schaut dass er net �ber max kommt
void watchdoge(Engines* en){
	int i=0;
	for(i=0;i<4;i++){
		if(en->set[i] < min_value)en->set[i]=min_value;
		if(en->set[i]>safetyenvelope){
			en->set[i]=safetyenvelope;
			en->err[i]++;
		}
		else en->err[i]=0;
	}
	for(i=0;i<4;i++){
		if((en->err[i])>err_count){
			sprintf(Engin_Watch_doge,"RErr::en%d::",(i+1));
			PB[0]=1;
		}
	}
}

void pre_cast(Engines* engine){
	double_to_unsigchar(engine->StellWert1,&(engine->set[0]));
	double_to_unsigchar(engine->StellWert2,&(engine->set[1]));
	double_to_unsigchar(engine->StellWert3,&(engine->set[2]));
	double_to_unsigchar(engine->StellWert4,&(engine->set[3]));
}


void setEngine(Engines* en){
	if(PB[1] && engine_on){
		pre_cast(en);
		watchdoge(en);
		set_engine(en->set[0],en->set[1],en->set[2],en->set[3]);
	}
}

void setTrim(regelungspara* para){
	para->trim_x+=para->nick_aim;
	para->trim_y+=para->roll_aim;
	para->nick_aim=0;
	para->roll_aim=0;
}

void dip_view_engines(Engines* en){
	char puffer[20];
	sprintf(puffer,"En1:%0.2lf %d",en->StellWert1,en->set[0]);
	dip_write_string(puffer,1);
	sprintf(puffer,"En2:%0.2lf %d",en->StellWert2,en->set[1]);
	dip_write_string(puffer,2);
	sprintf(puffer,"En3:%0.2lf %d",en->StellWert3,en->set[2]);
	dip_write_string(puffer,3);
	sprintf(puffer,"En4:%0.2lf %d",en->StellWert4,en->set[3]);
	dip_write_string(puffer,4);
}
